<?php

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Mvc\Model\Criteria;



class EventController extends ControllerBase 
{


    public function initialize(){
        parent::initialize();
    }
    public function sampleAction(){

         // This is an Ajax response so it doesn't generate any kind of view
         $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
            
 $cities = Cities::find(['limit' => '10']);
foreach ($cities as $city) {
 $array[] =  $city->city_name;
}
echo json_encode($array);

            
            // $cities = Cities::find(['limit' => '10']);



            // $array = array("david", "one", "three", "davidheart");
            // echo json_encode ($array); //Return the JSON Array

    }

  public function getCitiesAction() {
        if ($this->request->isAjax()) {
            $arrCities = array();
             $stateCode = $this->request->getQuery('param1');
            $cities = Cities::find("state_code_id='" . $stateCode . "'");

            foreach ($cities as $city) {
                $arrCities[] = ['id' => $city->id, 'name' => $city->city_name];
            }
            $payload     = $arrCities; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }





    public function getStatesAction() {


        if ($this->request->isAjax()) {
            $arrStates = array();
            $countryCode = $this->request->getQuery('param');


            $states = States::find("country_code_id='" . $countryCode . "'");

            foreach ($states as $state) {
                $arrStates[] = ['id' => $state->id, 'name' => $state->state_name];
            }

            $payload     = $arrStates; //array(1, 2, 3);
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

          

            return $response;
        }
    }


    /**
     * [flagAction description]
     * @return [type] [description]
     */
    public function flagAction(){

        $this->view->disable();

        if($this->request->isAjax()) 
        {
            $review = $this->request->getQuery('review');
            $reviewer = $this->request->getQuery('reviewer');
            $flagger = $this->request->getQuery('flagger');
            $flag_type = $this->request->getQuery('flag_type');
            $referer = $this->request->getHTTPReferer (); 
            $controller = 'classifieds';
            $newFlag = new Flags();
            $newFlag->created = date('Y-m-d H:i:s');
            $newFlag->modified = date('Y-m-d H:i:s');
            $newFlag->review_id = $review;
            $newFlag->member_id = $flagger;
            $newFlag->target_id = $reviewer;
            $newFlag->flag_type_id = $flag_type;
            $newFlag->location = $controller; 
            $newFlag->status = '0';
            $newFlag->page = $referer;

            if ($newFlag->create()) {
                $resultInfo = 'success';
            } else {
                $resultInfo = 'failed';
            }
            $arrayName = array('review' => $review, 'reviewer' => $reviewer, 'flagger' => $flagger, 'result' => $resultInfo);
            echo json_encode(array('result' => 'OK', 'items' => $arrayName));
        }

    }



    public function formValidate() {
        $validation = new Phalcon\Validation();
        $validation->add('name', new PresenceOf(array(
            'message' => 'name field is required'   
        )));
        // $validation->add('website', new PresenceOf(array(
        //     'message' => 'website field is required'    
        // )));
        $validation->add('telephone', new PresenceOf(array(
            'message' => 'telephone field is required'  
        )));
        $validation->add('event_info', new PresenceOf(array(
            'message' => 'event information field is required'  
        )));
        $validation->add('street', new PresenceOf(array(
            'message' => 'street field is required' 
        )));
        // $validation->add('city', new PresenceOf(array(
        //     'message' => 'city field is required'   
        // )));
        // $validation->add('country', new PresenceOf(array(
        //     'message' => 'city field is required'   
        // )));
        $validation->add('category', new PresenceOf(array(
            'message' => 'category field is required'   
        )));
        return $validation->validate($_POST);
    
    }


    /**
     * [loadMoreEvent description]
     * @return [type] [description]
     */
    public function load_ajaxAction(){

        // This is an Ajax response so it doesn't generate any kind of view
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

        //if ajax request run
        if ($this->request->isAjax()) {


            $id = $this->request->getQuery('id');
        // Events display order by descending 
            $events = Events::find([
            'conditions' => 'id < :id:',
            'bind' => array('id' => $id),
            'order' => 'id desc',
            'limit' => 6
            ]);



            if($events){
                $i = 1;

                $cc = count($events);

                foreach ($events as $event): 

              ?>
         
            <!-- 1st item events-->
                 
                  <div class="col-md-4 col-sm-6 matcher-height" >
                      <div class="panel panel-default item-box paperts" style="min-height:270px">
                       <a href="<?php echo $this->url->getBaseUri().'event/view/'. $event->id; ?>">
                        <div class="panel-thumbnail custom-thumbnail">

                        <?php
                            $eventPhoto = 'img/nophoto.jpg'; 
                            $rPhotoCount = count($event->EventPhotos);
                            if ($rPhotoCount > 0) {
                                $eventPhoto = $event->eventphotos[0]->file_path.$event->eventphotos[0]->filename;
                            }
                            echo Phalcon\Tag::image([$eventPhoto, 'class' => 'img-responsive']);
                        ?>

                        </div>
                             </a>
                        <div class="panel-body" style="text-center"><!-- start of panel body -->
                        <a href="<?php echo $this->url->getBaseUri().'event/view/'. $event->id; ?>">
                           <p class="lead ellipsis5"  data-toggle="tooltip" title="<?php echo $event->name; ?> "  >
                                     <?php echo $event->name;  ?> 
                           </p><!-- end of lead -->
                         </a>
                
                            <span  class="s3d designmoo">
                            <i class="fa fa-calendar"></i>

                                <?php
                                    $datess = ''; 
                                    $ccx = count($event->EventDatetimes);
                                    if ($ccx > 0) {
                                        $datess = $event->EventDatetimes[0]->date; 
                                    }
                                echo date('M d, Y ', strtotime($datess));
                                 ?>
                            
                             </span>
                            <div class="ellipsis2">
                                    
                                <?php 

                                    $ecity = '';
                                    $cities = Cities::findFirst("id = '$event->city' ");

                                    if($cities){
                                        echo '<span class="glyphicon glyphicon-map-marker"></span>';
                                        $ecity .= $cities->city_name;
                                        $ecity .= ', '. $cities->state_code_id;
                                        $ecity .= ', '.$cities->CountryCodes->country_name;
                                        echo $ecity;
                                    }

                                ?>

                                </div>
                        <!--  <p><small>date posted:  --><?php //echo date('M d, Y ', strtotime($event->created)); ?><!-- </small> </p> -->
                
        
                    
                    </div>

                      </div>
                  </div>
             
                  <!-- 1st item items events-->
                <?php endforeach; ?>

        <?php  if($cc > 6): ?>
            <div class="row">
            <div class="col-md-4 col-md-offset-4">
            <div class="form-group">
                        <div class="show_more_main" id="show_more_main<?php echo $event->id; ?>">
                            <span id="<?php echo $event->id; ?>" class="btn btn-primary show_more btn-block" title="Load more posts">Show more</span>
                            <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
                        </div>
            </div>
            </div>
            </div>
        <?php  endif; ?>
 
        <?php
            }
        }

        
    }


    /**
     * 
     * [indexAction description]
     * @return [type]
     */
    public function indexAction() {

        // set default1 template for new design template
        $this->view->setTemplateAfter('default1');
        // set breadcrum for page identification
        $this->view->setVar('breadcmp', 'Events');

        // EventsCategories and Countries  database select all
        // $categories = EventsCategories::find();
        // $countries  = Countries::find();

        // //set vars
        // $this->view->setVars([
        //     'categories' => $categories,
        //     'countries' => $countries
        // ]);


        $name = '';
        $ecity = '';
        $city_code = '';
        $cc = '';
        $trig = '';
        // Form Events (if Find Event click search [post == true] run this program)
        if ($this->request->isPost()) {


            $keyword = $this->request->getPost("name");
            $city_code = $this->request->getPost("city_code");
  
            $name = $keyword;
            if($city_code != ''){
                $cities = Cities::findFirst($city_code);
                $ecity .= $cities->city_name;
                $ecity .= ', '. $cities->state_code_id;
                $ecity .= ', '.$cities->CountryCodes->country_name;
            }
            if(!empty($keyword)) {
                $conditions .= ' name LIKE :name:';
                $bind['name'] = '%'.$keyword.'%';
            }
            $conditions .= ($keyword != '' AND $city_code != '')? 'AND' : '' ;
            if(!empty($city_code)) {
                $conditions .= ' city LIKE :city: ';
                $bind['city'] = '%'.$city_code.'%';
            }


            $parameters= ['conditions' => $conditions, 'bind' => $bind,'order' => 'id DESC', 'limit' => '12'];

            $qlimit = Events::count(['conditions' => $conditions, 'bind' => $bind]);
            if($qlimit < 12){
                $trig = 1;
            }
        }else{
                    // Events display order by descending 
             $parameters = array('order' => 'id DESC', 'limit' => '12');
            
        }


        //print_r($parameters);
        
         $autos = Events::find($parameters);
        $cc = count($autos);
        $this->view->setVars([
            'events' => $autos,
            'ename' => $name,
            'ecity' => $ecity,
            'ecity_code' => $city_code,
            'counter' => $cc,
            'trigger' => $trig
        ]);




    }//end of indexAction



    public function viewAction($id = null) {

        // set default1 template for new design template
        $this->view->setTemplateAfter('default1');
        
        $event = Events::findFirst($id);

        if (!$event) {
            return $this->response->redirect('event/index');
        }

        $this->view->setVar('event', $event);
    }

    public function newAction() {
        $this->view->setTemplateAfter('default1');



        if ($this->request->isPost()) {
   
    

        // $messages = $this->formValidate();
        // if (count($messages)) {
        //  $this->view->disable();
        //  $errorMsg = '';
        //  foreach ($messages as $msg) {
        //    $errorMsg .= $msg . "<br>";       
        //  }
        //     $this->session->set('errorMessasge', $errorMsg);
        //  return $this->response->redirect('event/new/'); 
        // } 



//                 $i = 0;
//                 foreach ($dates as $date) {

// echo $date.$event_start[$i].$event_end[$i]."</br>";

//       $i++;
                    // $eventDatetime = new EventDatetimes();
                    // $eventDatetime->event_id = $id;
                    // $eventDatetime->date = $date;
                    // $eventDatetime->from_time = $fromTimes[$i];
                    // $eventDatetime->to_time = $toTimes[$i];
                    // $i++;

                    // if (!$eventDatetime->create()) {
                    //     $this->view->disable();

                    //     echo "failed to insert event date and time in event_datetimes table. :(";
                    // }
                // }
            // address 
            $street = $this->request->getPost("street");
            $country_id = $this->request->getPost("country_id");
            $state = $this->request->getPost("state");
            $city = $this->request->getPost("city");

            //event information
            $userSession = $this->session->get("userSession");
            $eventName = $this->request->getPost("name");
            $website   = $this->request->getPost("website");
            $telephone = $this->request->getPost("telephone");
            $info      = $this->request->getPost("event_info");
            $category  = $this->request->getPost("category");

            // map location
            $latitude = $this->request->getPost("lat");
            $longitude = $this->request->getPost("lng");


            // schedule
            $dates = $this->request->getPost("date");
            $event_start = $this->request->getPost("event_start");
            $event_end = $this->request->getPost("event_end");


            $event = new Events();
            $event->created = date('Y-m-d H:i:s');
            $event->modified = date('Y-m-d H:i:s');
            $event->member_id = $userSession['id'];
            $event->name = $eventName;
            // $event->event_date = $this->request->getPost("date");
            $event->website = $website;
            $event->telephone = $telephone;
            $event->event_category_id = $category;
            $event->lng = $longitude;
            $event->lat = $latitude;
            $event->eventinfo = $info;

            $event->street = $street;
            $event->city = $city;
            $event->country_code_id = $country_id;
            $event->state = $state;

            if ($event->create()) {

               $i = 0;
                foreach ($dates as $date) {

                    $eventD = new EventDatetimes();
                    $eventD->event_id = $event->id;
                    $eventD->date = $date;
                    $eventD->from_time = $event_start[$i];
                    $eventD->to_time = $event_end[$i];
                   $i++;
                    if (!$eventD->create()) {
                        $this->session->set('errorMessasge', 'failed to insert event date and time');
                    }

                }

                if($this->request->hasFiles() == true) {
                    // $this->folderOrganizer('classfieds', $id);
                    $uploads = $this->request->getUploadedFiles();

                    // upload default to false
                    $isUploaded = false;

                    //allowed file extension
                    $fileImageExt = array('jpeg', 'jpg', 'png');

                    $filePath = 'img/event/';

                    $fileType = '';

                    foreach ($uploads as $upload) {

                        $fileName = $upload->getname() ;

                        $fileInfo = new SplFileInfo($fileName);

                        $fileExt  = strtolower($fileInfo->getExtension());

                        // filename convert to md5
                        $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'-evt.'.$fileExt;

                        $path = NULL;

                        //
                        if(in_array($fileExt, $fileImageExt)){
                                $path = 'img/event/'.$newFileName;
                        }

                       //move the file and simultaneously check if everything was ok
                       ($path != NULL OR !empty($path)) ? $isUploaded = true : $isUploaded = false;

                        if ($isUploaded) {
                            $thingPhotos = new EventPhotos();
                            $thingPhotos->created = date('Y-m-d H:i:s');
                            $thingPhotos->modified = date('Y-m-d H:i:s');
                            $thingPhotos->member_id = $userSession['id'];
                            $thingPhotos->event_id = $event->id;
                            $thingPhotos->file_path = $filePath;
                            $thingPhotos->filename = $newFileName;
                            $eventPhotos->caption = 'caption';
                            if ($thingPhotos->create()) {
                                $upload->moveTo($path);
                            } else {
                                $this->session->set('errorMessasge', 'failed add image');
                                //print_r($thingPhotos->getMessages());
                            }
                        }

                    }

            
                }




                
                $this->response->redirect('event/view/'.$event->id);
            }else{
                foreach ($event->getMessages() as  $error) {
                    $errorMsg .= $error->getMessage()."</br>";
                }
               $this->session->set('errorMessasge', $errorMsg);
            }

            


           // return $this->response->redirect('event/view/'.$event->id);
        }

        $events = Events::find();
        $categories = EventsCategories::find();
        $countries = Countries::find();
        $this->view->setVar('events', $events);
        $this->view->setVars([
            'events' => $events,
            'categories' => $categories,
            'countries' => $countries
        ]);
    }

   public function updateAction($id = null) {
        $this->view->setTemplateAfter('default1');

        $path = 'img/event/';

        if ($this->request->isPost()) {

          $userSession = $this->session->get("userSession");
                if ($this->request->hasFiles() == true){
                    set_time_limit(1200);
                    $uploads = $this->request->getUploadedFiles();

                    $isUploaded = false;

                    foreach($uploads as $upload) {
                        $imageName = $upload->getKey(); // $this->request->getPost('image', $ctr);
                        $inputRow = str_replace('image', '', $imageName);

                        $checker = EventPhotos::findFirst("event_id = '$id' ");

                        $unlink = $checker->filename;
                        # define a "unique" name and a path to where our file must go
                        $fileName = $upload->getName();
                        $fileInfo = new SplFileInfo($fileName);

                        $fileExt  = strtolower($fileInfo->getExtension());

                        $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'ext1.'.$fileExt;
                            //$fileExt = $upload->getExtension();
                        $fileImageExt = array('jpeg', 'jpg', 'png');
                        $fileType = '';
                        $filePath = '';
                        $path = '';
                         if(in_array($fileExt, $fileImageExt)){
                                $path = 'img/event/'.$newFileName;
                                $filePath = 'img/event/';
                                //$fileType = 'Image';
                        }

                        #move the file and simultaneously check if everything was ok
                        ($upload->moveTo($path)) ? $isUploaded = true : $isUploaded = false;

                        if($isUploaded) {
                                if($checker){
                                    $realtyPhotos = EventPhotos::findFirst("event_id = '$id' ");
                                    $realtyPhotos->modified = date('Y-m-d H:i:s'); 
                                    $realtyPhotos->member_id =  $userSession['id'];
                                    $realtyPhotos->file_path =  $filePath;
                                    $realtyPhotos->filename =  $newFileName;
                                    if($realtyPhotos->update()){
                                        unlink($filePath.$unlink);
                                    }

                                } else {
                                    $realtyPhotos = new EventPhotos();
                                    $realtyPhotos->created = date('Y-m-d H:i:s'); 
                                    $realtyPhotos->modified = date('Y-m-d H:i:s'); 
                                    $realtyPhotos->member_id =  $userSession['id'];
                                    $realtyPhotos->event_id =  $id;
                                    $realtyPhotos->file_path =  $filePath;
                                    $realtyPhotos->filename =  $newFileName;
                                    $realtyPhotos->create();
                                }
                            } // isUploaded
                    } // foreach uploads

                } // has files

            $messages = $this->formValidate();
            if (count($messages)) {
                $this->view->disable();
                $errorMsg = '';
                foreach ($messages as $msg) {
                  $errorMsg .= $msg . "<br>";       
                }
                   $this->session->set('errorMessasge', $errorMsg);
               // return $this->response->redirect('event/'); 
            } 
            $eventdateid = $this->request->getPost("eventdateid");
            $date = $this->request->getPost("date");
            $event_start = $this->request->getPost("event_start");
            $event_end = $this->request->getPost("event_end");





            $c = count($eventdateid);
            for ($i=0; $i < $c; $i++) { 
                $eventdateid[$i];
                $eventdates = EventDatetimes::findFirst($eventdateid[$i]);
                $eventdates->date = $date[$i];
                $eventdates->from_time = $event_start[$i];
                $eventdates->to_time = $event_end[$i];
                $eventdates->update();
            }

            $userSession = $this->session->get("userSession");

            $event = Events::findFirst($id);
            $event->modified = date('Y-m-d H:i:s');
            $event->member_id = $userSession['id'];
            $event->name = $this->request->getPost("name");
            $event->event_date = $this->request->getPost("date");
            $event->website = $this->request->getPost("website");
            $event->telephone = $this->request->getPost("telephone");
            $event->street = $this->request->getPost("street");
            $event->state = $this->request->getPost("state");
            $event->city = $this->request->getPost("city");
            $event->country_code_id = $this->request->getPost("country_id");
            $event->event_category_id = $this->request->getPost("category");
            $event->eventinfo = $this->request->getPost("event_info");

            if ($event->update()) {
         
                return $this->response->redirect('event/update/'.$id);
            } else {
            

                print_r($event->getMessages());
                    $errorMsg = '';
                foreach ($event->getMessages() as  $error) {
                    $errorMsg .= $error->getMessage()."</br>";
                }
               $this->session->set('errorMessasge', $errorMsg);

            }

          //  $this->view->disable();
        }



        $event = Events::findFirst($id);
        $this->view->setVar('event', $event);
        $eventPhotos = EventPhotos::find("event_id = $id");
        $this->view->setVar('eventPhotos', $eventPhotos);

    }


    public function searchAction() {
        // get latitude and longitude via google maps api service. 
        $request =$this->request;
        $this->view->disable();
        $response = new \Phalcon\Http\Response();

        $res = $_GET["location"]; // $_GET parameter
        $address = urlencode($res);
                    
        $Response = array('success' => $res);
        $url = "https://maps.google.com/maps/api/geocode/json?sensor=true&address=$address&key=AIzaSyBSR4ZUyKhxVcN3eMNzQWnm8YSdP-KE8uM";
        $Res = file_get_contents($url);
        $resp = json_decode($Res, true);
        $lat = $resp['results'][0]['geometry']['location']['lat'];
        $lng = $resp['results'][0]['geometry']['location']['lng'];
        $res = array("lat" => $lat, "lng"=> $lng );
        $response->setContent(json_encode($res));
        $response->setContentType('application/json', 'UTF-8');
        return $response;
    }



    public function listAction() {

        $userSession = $this->session->get("userSession");
        $member_id = $userSession['id'];
        $events = Events::find("member_id = '$member_id' ");
        $this->view->setVar('events', $events);
    }




    public function delete_listAction($id = null){

         $query = Events::find($id);
        // $idphoto = $query->AutomotivePhotos->id;
        $name = $query->name;
        if ($query->delete()) {

            $this->session->set('successMessasge', $name.'succesfully deleted');
        }else{
            $this->session->set('errorMessasge', 'A problem has been occurred while deleting your data');
        }
        $this->response->redirect('event/list');
    }





}
