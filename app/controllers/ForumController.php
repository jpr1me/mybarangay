<?php
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;
use Helpers\ZoidHelpers; 

class ForumController extends ControllerBase {

    /**
     * [initialize description]
     * @return [type] [description]
     */
    public function initialize(){
        parent::initialize();
        //  the same of
        //  $forum_category = WhatsupCategories::find();
        //  setVar(forum_category, $forum_category)
        $this->view->forum_category = WhatsupCategories::find();
        $this->view->userSession = $this->session->get("userSession"); 
        if (!isset($this->persistent->link)) {
           $this->view->linkfx = $this->persistent->link = 'z_all';
         }else{
           $this->view->linkfx = $this->persistent->link;
         }

        if (!isset($this->persistent->view_id)) {
           $this->view->view_id = $this->persistent->view_id = 0;
         }else{
           $this->view->view_id = $this->persistent->view_id;
         }
    }

    /**
     * [persistentAction description]
     * @return [type] [description]
     */
    public function persistentAction(){
        $this->view->disable();
        if($this->request->isAjax()){

            $perst = $this->request->getPost('perst');
            $linkx  = false;
                if(in_array($perst, ['z_all','z_myt','z_adt','z_view','z_lat','z_pop'])){
                $linkx  = true;
            }
            $perstx = ($linkx == false)? 'z_all' : $perst;
            $this->persistent->link = $perstx;
            $zoid = json_encode(array('persitent' => $perstx ));
            die($zoid); 
        }
    }



    /**
     * [indexAction description]
     * @return [type] [description]
     */
    public function indexAction(){
        $currentPage = isset($_GET["page"])? (int) $_GET["page"] : 1 ;
        // $currentPage = $this->request->getQuery("page", "int");

        // Pagination for Index Forum page Limit by 10
        $whatsupTopicsResult = WhatsupTopics::find(["order" => "id DESC"]);

        $paginator = new Paginator(
            [
                "data" => $whatsupTopicsResult,
                "limit"=> 10,
                "page" => $currentPage
            ]
        );
        //paginator
        $page = $paginator->getPaginate();
        // count of rows
        $this->view->setVar('counter', $whatsupTopicsResult);
        // results of paginate use foreach (  $whatsupTopicsResult->items )
        $this->view->setVar('whatsupTopicsResult', $page);
    }

    /**
     * [pageModeratorAction description]
     * @return [type] [description]
     */
    protected function pageModeratorAction($id = null){
        $this->view->disable();
        if($this->request->isAjax()) {

        }
    }


    /**
     * [loadMoreEvent description]
     * @return [type] [description]
     */
    public function load_ajaxAction(){

        // This is an Ajax response so it doesn't generate any kind of view
        $this->view->disable();
        //if ajax request run
        if ($this->request->isAjax()) {

            //last requested id
            $id = $this->request->getQuery('idx');
            $catg = $this->request->getQuery('catg');
            $sort = $this->request->getQuery('sort');
            $view = $this->request->getQuery('view');

            $arc = ($sort == 'asc')? '>' : '<' ;

            $cond = '';
            $bind =  [];

            if($catg != null OR $catg != '' OR !empty($catg)){
                $cond .= ' whatsup_category_id = :ids: AND ';
                $bind['ids'] = $catg;
            }
            $cond .= ' id '.$arc.' :id:';
            $bind['id'] = $id;
            $topics = WhatsupTopics::find([
            'conditions' => $cond ,
            'bind' => $bind,
            'limit' => 5,
            'order' => 'id '.$sort
            ]);

            // $topics = WhatsupTopics::find([
            // 'conditions' => 'id < :id:',
            // 'bind' => array('id' => $id),
            // 'order' => 'id desc',
            // 'limit' => 6
            // ]);
        $setup = array('sort' => $sort, 'view' => $view);
          return $this->postModerator($topics, $catg, $setup );
        }//end of request ajax

    }


    protected function categoryName($cat){

             switch ($cat){
                case '1':
                  $ct = 'Food & Nightlife';
                break;
                case '2':
                  $ct = 'Travel & Lifestyle';
                break;
                case '3':
                  $ct = 'Events & Happenings';
                break;
                case '4':
                  $ct = 'Entertainment & Pop Culture';
                break;
                case '5':
                  $ct = 'Fashion & Shopping';
                break;
                case '8':
                  $ct = 'Pinoy News & Culture';
                break;
                case '9':
                  $ct = 'Site Questions & Updates';
                break;
                default:
                  $ct = 'All';
                break;
              }
        return $ct;
    }



    /**
     * [get_timeago description]
     * @param  [type] $ptime [description]
     * @return [type]        [description]
     */
    protected function get_timeago( $ptime ){
        $estimate_time = time() - $ptime;

        if( $estimate_time < 1 ){
            return 'less than 1 second ago';
        }
        $condition = array(
                    12 * 30 * 24 * 60 * 60  =>  'year',
                    30 * 24 * 60 * 60       =>  'month',
                    24 * 60 * 60            =>  'day',
                    60 * 60                 =>  'hour',
                    60                      =>  'minute',
                    1                       =>  'second'
        );
        foreach( $condition as $secs => $str )
        {
            $d = $estimate_time / $secs;
            if( $d >= 1 )
            {
                $r = round( $d );
                return  $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
            }
        }
    }


    /**
     * [postModerator description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    protected function postModerator($query = null, $cat = null, $setup) {
        //if query has row
        $options = [];
        $options['offload'] = 0;
         $html = '';
        if(count($query)){

           
            $bind = [];
            $cond = '';
            // $html .= '<script>';
            // $html .= '$(document).ready(function() { $("time.timeago").timeago(); });';
            // $html .= '</script>';

            $catChecker = in_array($cat, ['1','2','3','4','5','6','7','8','9']);

            $ct = $this->categoryName($cat);
            //---------------------------------------------------------
            if($setup['view'] == 2){
                foreach ($query as $topic) {
                $html .= '<div class="post">';
                  $html .= '<div class="wrap-ut pull-left">';
                      $html .= '<div class="userinfo pull-left">';
                          $html .= '<div class="avatar">';
                                $profilePic = 'http://placehold.it/50x50&text=NO+PROFILE+PIC';
                                $avatar = MemberPhotos::findFirst('member_id = "'.$topic->member_id.'" and primary_pic="Yes"');
                                if ($avatar) {
                                    $profilePic =  'http://mybarangay.com'.$this->url->getBaseUri() . $avatar->file_path . $avatar->filename;
                                }
                                $html .= '<a class="restricted" href="'.$this->url->getBaseUri().'"member/page/"'.$topic->member_id.'>';
                                $html .= Phalcon\Tag::image(array($profilePic,  'style' => 'width:30px;height:30px'));
                                $html .= '</a>';
                          $html .= '</div>';
                     $html .= ' </div>';
                      $html .= '<div class="posttext pull-left">';
                          $html .= '<h2><a class="viewlink" href="'.$topic->id.'" >'.$topic->subject.'</a></h2>';
                          $html .= '<small>'. ($ct == 'all' )? $this->categoryName($topic->whatsup_category_id) : ''  .'</small>';
          
                      $html .= '</div>';
                      $html .= '<div class="clearfix"></div>';
                  $html .= '</div>';

                      $html .= '<div class="postinfo pull-left" style="border:none">';
                        $html .= '<div class="time"><i class="fa fa-clock-o"></i> ';
                        $dates = $this->get_timeago(strtotime($topic->created_at));
                                $html .= '<time >'.$dates.'</time>';
                        $html .= '</div>';;                               
                      $html .= '</div>';
                      $html .= '<div class="clearfix"></div>';
                $html .= '</div>';
                }
            }else{
            //---------------------------------------------------------
                foreach ($query as $topic) {
                    $html .= '<div class="post">';//start of post
                    // $html .= $topic->id;
                      $html .= '<div class="wrap-ut pull-left">';//start of wrap-ut
                         //---------------------left side avatar--------------------------
                         $html .= '<div class="userinfo pull-left">';//start of userinfo
                             $html .= '<div class="avatar">';//start of avatar

                                $profilePic = 'http://placehold.it/50x50&text=NO+PROFILE+PIC';
                                $avatar = MemberPhotos::findFirst('member_id = "'.$topic->member_id.'" and primary_pic="Yes"');
                                if ($avatar) {
                                    $profilePic =  'http://mybarangay.com'.$this->url->getBaseUri() . $avatar->file_path . $avatar->filename;
                                }
                                $html .= '<a class="restricted" href="'.$this->url->getBaseUri().'"member/page/"'.$topic->member_id.'>';
                                $html .= Phalcon\Tag::image(array($profilePic,  'style' => 'width:50px;height:50px'));
                                $html .= '</a>';

                             $html .= '</div>';//end of avatar
                         $html .= '</div>';//end of userinfo
                         //----------------------------------------------------------------

                        //---------------------middle content of post----------------------
                        $html .= '<div class="posttext pull-left">';//start of posttext
                            $html .= '<h2><a href="'.$this->url->getBaseUri().'forum/view/'.$topic->id.'" >'.$topic->subject.'</a></h2>';
                          $html .= '<small style="font-size:9px;">'. ($ct == 'all' )? $this->categoryName($topic->whatsup_category_id) : ''  .'</small>';

                            $html .= '<div class="content-post" style="margin-top:10px;">';//start of content
                                $string = strip_tags($topic->content);
                                if (strlen($string) > 190) {
                                    // truncate string
                                    $stringCut = substr($string, 0, 190);
                                    // make sure it ends in a word so assassinate doesn't become ass...
                                    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="'.$this->url->getBaseUri().'forum/view/'.$topic->id.'">Read More</a>'; 
                                }
                                $html .= $string;
                            $html .= '</div>';//end of content

                         $html .= '</div>';//end of posttext
                        //----------------------------------------------------------------

                        $html .= '<div class="clearfix"></div>';//S-E of clearfix
                       $html .= '</div>';//end of wrap-ut

                    //---------------------right information of post----------------------
                    $html .= '<div class="postinfo pull-left">';//start of postinfo
                        $html .= '<div class="comments">';//start of comments
                            $html .= '<div class="commentbg">';
                                $html .= WhatsupReplies::count("whatsup_topic_id = '$topic->id' "); 
                              //$html .= '<div class="mark"></div>';
                            $html .= '</div>';
                        $html .= '<span style="padding:5px 5px;margin:0px;postion:realtive;text-align:center;clear:both">comment</span>';
                        $html .= '</div>';//end of comments


                        $html .= '<div class="views"><a href="'.$this->url->getBaseUri().'forum/view/'.$topic->id.'"><i class="glyphicon glyphicon-share-alt"></i> reply</a></div>';
                        $html .= '<div class="time"><i class="fa fa-clock-o"></i> ';
                          $dates = $this->get_timeago(strtotime($topic->created_at));
                                  $html .= '<time >'.$dates.'</time>';
                        $html .= '</div>';
                    $html .= '</div>';//end of postinfo
                    //---------------------------------------------------------------------

                       $html .= '<div class="clearfix"></div>';//S-E of clearfix
                    $html .= '</div>';//end of post
                }
            }//end of else
            //---------------------------------------------------------


            if(!empty($setup['search'])){
                $cond .= ' subject LIKE :key: AND';
                $bind['key'] = '%'.$setup['search'].'%';            
            }

            if($catChecker){
                $cond .= ' whatsup_category_id = :ids: AND ';
                $bind['ids'] = $cat;
            }else{
                $cat = 1;
            }
            $arc = ($setup['sort'] == 'asc')? '>' : '<' ;
            $cond .= ' id '.$arc.' :id:';
            // $cond .= 'id < :id:';
            $bind['id'] = $topic->id;

            $cctop = WhatsupTopics::count([
            'conditions' => $cond ,
            'bind' => $bind
            ]);

            $options['offload'] = $cctop;
            $options['cat_id'] = $cat;

            $zoid = json_encode(array( 'type'=>'ok', 'id' => $topic->id ,'text' => $html, 'option' => $options ));
            die($zoid); //exit script outputting json data if topic not empty

        //if query empty
        }else{

                $html .= '<div class="post">';
                $html .= '<h4 style="padding:15px 20px">No Results Found</h4>';
                $html .= '</div>';

            $zoid = json_encode(array( 'type'=>'not', 'id' => '', 'text' => $html, 'option' => $options ));
            die($zoid); //exit script outputting json data if empty
        }

    }//end of postModerator


    public function testAction($id = null, $page = 1) {

        // This is an Ajax response so it doesn't generate any kind of view
        //$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $this->view->disable();
        if($this->request->isAjax()) {
        $currentPage = $page;
        $sort = 'desc';
        
        $catg = $this->request->getQuery('catg');
        $sort = $this->request->getQuery('sort');
        $view = $this->request->getQuery('view');
        // if($id == '' or $id > 9 or $id == null or $id <= 0){
        //     $id = '';
        // }
        switch ($catg) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 8:
            case 9:
                $cond = array('conditions' => 'whatsup_category_id = :id:','bind' => array('id' => $catg),'order' => 'id '.$sort);
            break;
            default:
                 $cond = array('order' => 'id '.$sort);
            break;
        }
        // Pagination for Index Forum page Limit by 10
        $whatsupTopicsResult = WhatsupTopics::find($cond);
        $paginator = new Paginator(
            [
                "data" => $whatsupTopicsResult,
                "limit"=> 10,
                "page" => $currentPage
            ]
        );
        $page = $paginator->getPaginate();
        $setup = array('sort' => $sort, 'view' => $view);
        return $this->postModerator($page->items, $catg, $setup);


// if(count($page->items) ){
//       $ccount = 1; foreach ($page->items as $topic) {
//         $html .= '<div class="post">';
//             $html .= '<div class="wrap-ut pull-left">';
//                 $html .= '<div class="userinfo pull-left">';
//                     $html .= '<div class="avatar">';
//                                                 // profile picture 
//                         $profilePic = 'http://placehold.it/50x50&text=NO+PROFILE+PIC';
//                         $avatar = MemberPhotos::findFirst('member_id = "'.$topic->member_id.'" and primary_pic="Yes"');
//                         if ($avatar) {
//                             $profilePic =  'http://mybarangay.com'.$this->url->getBaseUri() . $avatar->file_path . $avatar->filename;
//                         }

//                         $html .= '<a class="restricted" href="'.$this->url->getBaseUri().'"member/page/"'.$topic->member_id.'>';
//                         $html .= Phalcon\Tag::image(array($profilePic,  'style' => 'width:50px;height:50px'));
//                         $html .= '</a>';

//                         $html .= '<div class="status green">&nbsp;</div>';

//                         $html .= '</div>';
//                         $html .= '<div class="icons">';
//                                         if (isset($userSession['id'])) {
//                                              if($userSession['id']) { 
//                                                 $topic->member_id;
//                                                 $html .= 'dav';
//                                                 $html .= '<span id="flagRemover'.$ccount.'">';
//                                                     $countFlag = Flags::count("member_id='".$topic->member_id."' and target_id='".$topic->id."' and review_id='".$topic->id."'");
//                                                     $checkFlag = Flags::find("member_id='".$topic->member_id."' and target_id='".$topic->id."' and review_id='".$topic->id."'");
//                                                         if ($countFlag == 0) { 
//                                                                 $html .= '<span id="flagSpan" class="glyphicon glyphicon-flag" style="float:right;cursor:pointer;" data-placement="top" onclick="flagAll('.$topic->member_id.','.$topic->id.','.$topic->id.','.$ccount.')" title="flag if inapproriate" data-toggle="modal" data-target="#flagModal"></span>';
//                                                         } else { 
//                                                             $html .= '<span style="float:right;color:red;">reported</span>';
//                                                         } 
//                                                 $html .= '</span>';
//                                             } 
//                                         } 

//                                                 $html .= '<img src="images/icon1.jpg" alt=""><img src="images/icon4.jpg" alt="">';
//                                             $html .= '</div>';
//                         $html .= '</div>';
//                                         $html .= '<div class="posttext pull-left">';
//                                             $html .= '<h2><a href="'.$this->url->getBaseUri().'forum/view/'.$topic->id.'" >'.$topic->subject.'</a></h2>';
                                         


//                                             // $html .= '<div id="forum-comp-count'.$ccount.'">';
                                             
//                                             //     $countLike = WhatsupCompliments::count("whatsup_topic_id='".$topic->id."' and rate='0'");
//                                             //     $countHelp = WhatsupCompliments::count("whatsup_topic_id='".$topic->id."' and rate='1'");
//                                             //     $countAwesome = WhatsupCompliments::count("whatsup_topic_id='".$topic->id."' and rate='2'");
//                                             //     if ($countLike > 0) { 
//                                             //     $html .= '<label class="colorBlue likeComp'.$ccount.'" data-toggle="tooltip" data-placement="top" title="LIKE"><img style="width: 15px;padding-bottom: 5px; " src="'.$this->url->getBaseUri(). 'img/Like.png"/><b class="colorRed"></b>';
//                                             //             $html .= '<span id="lykCount'.$ccount.'">'.$countLike.'</span>';
//                                             //             $html .= '</label>';
//                                             //     } 
//                                             //     if ($countHelp > 0){
//                                             //     $html .= '&nbsp;&nbsp;<label class="colorBlue helpComp'.$ccount.'" data-toggle="tooltip" data-placement="top" title="HELPFUL"><img style="width: 11px;" src="'.$this->url->getBaseUri(). 'img/Helpful.png"/><b class="colorRed"></b>';
//                                             //             $html .= '<span id="helpCount'.$ccount.'">'.$countHelp.'</span>';
//                                             //             $html .= '</label>';
//                                             //     }
//                                             //     if ($countAwesome > 0) {
//                                             //     $html .= '&nbsp;&nbsp;<label class="colorBlue awesomeComp'.$ccount.'" data-toggle="tooltip" data-placement="top" title="AWESOME"><img style="width: 12px;" src="'.$this->url->getBaseUri(). 'img/Rockon.png"/><b class="colorRed"></b>';
//                                             //             $html .= '<span id="aweCount'.$ccount.'">'.$countAwesome.'</span>';
//                                             //             $html .= '</label>';
//                                             //     }
                                            
//                                             // $html .= '</div>';

//                                             $html .= '<div class="content-post">';

//                                                 $string = strip_tags($topic->content);
//                                                 if (strlen($string) > 190) {
//                                                     // truncate string
//                                                     $stringCut = substr($string, 0, 190);
//                                                     // make sure it ends in a word so assassinate doesn't become ass...
//                                                     $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="'.$this->url->getBaseUri().'forum/view/'.$topic->id.'">Read More</a>'; 
//                                                 }
//                                                 $html .= $string;

//                                             $html .= '</div>';

//                                              $html .= '<div id="forum-compliment'.$ccount.'">';
 
//                                              $html .= '</div>';
//                                         $html .= '</div>';
//                                         $html .= '<div class="clearfix"></div>';
//                                     $html .= '</div>';
//                                     $html .= '<div class="postinfo pull-left">';

//                                         $html .= '<div class="comments">';
//                                             $html .= '<div class="commentbg">';
//                                                 $html .= WhatsupReplies::count("whatsup_topic_id = '$topic->id' "); 
//                                               //$html .= '<div class="mark"></div>';

                                              
//                                             $html .= '</div>';
//                                         $html .= '<span style="padding:5px 5px;margin:0px;postion:realtive;text-align:center;clear:both">comment</span>';
//                                         $html .= '</div>';


//                                         $html .= '<div class="views"><a href="'.$this->url->getBaseUri().'forum/view/'.$topic->id.'"><i class="glyphicon glyphicon-share-alt"></i> reply</a></div>';
//                                         $html .= '<div class="time"><i class="fa fa-clock-o"></i> ';
//                                             $html .= '<time class="timeago" datetime="'.$topic->created_at.'"></time>';
//                                         $html .= '</div>';

//                                     $html .= '</div>';

//                                     $html .= '<div class="clearfix"></div>';
//                                  $html .= '</div>';

//                             $ccount++; }


//                             // //start of pagination
//                             // $html .= '<div class="col-lg-12 col-xs-12 col-md-12" style=" padding:0px;margin;0px">';
//                             // $html .= '<ul class="pagination">';
//                             // $html .= '<li><a class="btn " href="javascript:void(0);" onclick="ajaxPage('.$id.', 1)"  ><i class="icon-fast-backward"></i> First</a></li>';
//                             // $html .= '<li><a class="btn " href="javascript:void(0);" onclick="ajaxPage('.$id.','.$page->before.')" ><i class="icon-step-beackward"></i> Previous</a></li>';
//                             //   // NEXT BUTTON
//                             //   if ($page->current < $page->total_pages){
//                             //         $act = '';
//                             //         $disb = '';
//                             //   }else{
//                             //         $act = 'active';
//                             //         $disb = '';
//                             //   }
//                             // $html .= '<li><a class="btn '.$disb.'  " href="javascript:void(0);" onclick="ajaxPage('.$id.','.$page->next.')"  ><i class="icon-step-forward"></i> Next</a></li>';
//                             // $html .= '<li><a class="btn '.$act.'    '.$disb.'" href="javascript:void(0);" onclick="ajaxPage('.$id.','.$page->last.')" ><i class="icon-fast-forward"></i> Last</a></li>';
//                             // $html .= '<li><span class="help-inline">'.$page->current.'/'.$page->total_pages .'</span></li>';
//                             // $html .= '</ul>';
//                             // $html .= '<div class="clearfix"></div>';
//                             // $html .= '</div>';//end of pagination


    
//         // $output =  json_encode(array('type' => 'success', 'item1' => $html));die($output);

//         $output = json_encode(array( //create JSON data
//             'type'=>'success', 
//              'id' => $topic->id,
//             'text' => $html
//         ));
//         die($output); //exit script outputting json data
//     }else{
//         $output = json_encode(array( //create JSON data
//             'type'=>'success', 
//              'id' => '0',
//             'text' => 'empty'
//         ));
//         die($output); //exit script outputting json data

// }





        }
    }
    public function complimentAction() {
    if($this->request->isAjax()) {
            $userSession = $this->session->get("userSession");
            $arrCompliment = array();

            $rate = $this->request->getQuery('rate');
            $topicId = $this->request->getQuery('topicId');
            $counter = $this->request->getQuery('counter');
            $memberId = $userSession['id'];

            $newComp = new WhatsupCompliments();
            $newComp->created = date('Y-m-d H:i:s');
            $newComp->modified = date('Y-m-d H:i:s');
            $newComp->rate = (int) $rate;
            $newComp->member_id = $memberId;
            $newComp->whatsup_topic_id = $topicId;
            $newComp->create();
            
            $countComp = WhatsupCompliments::count("whatsup_topic_id='".$topicId."' and rate='".$rate."'");
            $arrCompliment[] = ["rate" => $rate, "count" => $countComp];
       
            $payload     = $arrCompliment; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;

        } else {
            $this->view->disable();
            $html .= "flag action failed";
        }
    }

    /**
     * [indexAction description]
     * @return [type] [description]
     */
    public function viewAction($id = null){

        // user session ID
        $userSession = $this->session->get("userSession"); 


        //$this->view->disable();
        if($id == null){
            $this->view->noid = "please leave the page and return again(PROBLEM TOPIC)";
            $this->view->whatsupTopics = "";
        }else{
           $this->view->whatsupTopics = WhatsupTopics::findFirstById($id); 
        }

        $this->view->topicReplies = WhatsupReplies::find([
            "conditions" => "whatsup_topic_id = '$id' ",
            "order" => "id asc"
        ]);


        // if user post a reply run this code
        if ($this->request->isPost()) {
            $replies = new WhatsupReplies();
            $replies->content = $this->request->getPost('content');
            $replies->created_at = date('Y-m-d H:i:s');
            $replies->modified_at = date('Y-m-d H:i:s');
            $replies->whatsup_topic_id = $this->request->getPost('topic_id');
            $replies->member_id = $userSession['id'];
            // if true create reply
            if($replies->create()){
                       
                $this->response->redirect('forum/view/'.$replies->whatsup_topic_id );

            //else print error
            }else{
                print_r($replies->getMessages());
                $this->view->disable();
            }

        }// end of request post

    }//end of function viewAction


    public function view_editAction($id = null){

      $this->view->whatsupTopics = WhatsupTopics::findFirstById($id); 

      if($this->request->isPost()){
        // $this->view->disable();

          $userSession = $this->session->get("userSession"); 
          $member_id = $userSession['id'];
              

          $updTopic = WhatsupTopics::findFirst($id);
          $updTopic->subject = $this->request->getPost('subject');
          $updTopic->content = $this->request->getPost('content');
          $updTopic->whatsup_category_id = $this->request->getPost('whatsup_category_id');
          if($updTopic->update()){
            $this->response->redirect('forum/view_edit/'.$updTopic->id);
          }else{
            $errs = '';
            foreach ($updTopic->getMessages() as $err) {
              echo $errs .= $err->getMessage() .'</br>';
            }
            $this->view->disable();
          }
      }

    }





    protected function alert($info = null, $bool = FALSE){
      $msg = '';

      if($bool == TRUE){
        $msg .= '<div class="alert alert-success fade in">';
            $msg .= '<a href="#" class="close" data-dismiss="alert">&times;</a>';
            $msg .= '<strong>Success! </strong> ' . $info;
        $msg .= '</div>';
      }

      if($bool == FALSE){
        $msg .= '<div class="alert alert-danger fade in">';
            $msg .= '<a href="#" class="close" data-dismiss="alert">&times;</a>';
            $msg .= '<strong>Error! </strong> ' . $info;
        $msg .= '</div>';
      }

      return $msg;
    }



    /**
     * [listTopicAction description]
     * @return [type] [description]
     */
    public function list_topicAction(){
        $userSession = $this->session->get("userSession"); 
        if (!isset($userSession['id']) or empty($userSession['id'])) {
                return false;
        }
        $id = $userSession['id'];
     
        $this->view->toplist = WhatsupTopics::find("member_id = '$id' ");
        $hm = '';

        // $output = json_encode(array('type'=>'ok', 'text' => $hm));
        // die($output); //exit script outputting json data

 
    }

    public function deleteTopicAction(){

      $this->view->disable();
      if($this->request->isAjax()){
          $res = '';
          $msg = '';
        $id = $this->request->getQuery('id');

          $reply = WhatsupTopics::findFirst($id);
          if($reply->delete()){

            $res = 'ok';
            $msg .= $this->alert('Topic Successuly deleted', TRUE);

          }else{
             $res = 'failed';
             $msg .= $this->alert('Error deleting topic', TRUE);
          }

        $output = json_encode(array('type'=> $res, 'message' => $msg));
        die($output); //exit script outputting json data


      }

    }

    public function deleteReplyTopicAction(){

      $this->view->disable();
      if($this->request->isAjax()){
          $res = '';
        $id = $this->request->getQuery('id');
   
          $reply = WhatsupReplies::findFirst($id);
          if($reply->delete()){
            $res .= 'success';
          }else{
            $res .= 'error';
          }
        $output = json_encode(array('type'=> 'success', 'message' => 'mensahe'));
        die($output); //exit script outputting json data



      }
    }


    protected function editReplyTopicAction(){
      $this->view->disable();
      if($this->request->isAjax()){


      }
    }

    protected function sessionCheckerAction(){
      $this->view->disable();
      if($this->request->isAjax()){

      }
    }


    /**
     * [viewTopicAction description]
     * @return [type] [description]
     */
    public function viewTopicAction(){
    $userSession = $this->session->get("userSession"); 
       $this->view->disable();
        if($this->request->isAjax()) {
           $id = $this->request->getQuery('id');
           $this->persistent->view_id = $id;
           $whatsupTopics = WhatsupTopics::findFirstById($id); 

            $topicReplies = WhatsupReplies::find([
                "conditions" => "whatsup_topic_id = '$id' ",
                "order" => "id asc"
            ]);

            $hm ='';
            $hm .= '<div class="post ">';//start of beforepagination

                $hm .= '<div class="topwrap">';//start of topwrap
                    //------------------------------------------------------------
                    $hm .= '<div class="userinfo pull-left">';//start of userinfo
                        $hm .= '<div class="avatar">';//start of avatar
                            //$hm .= '<img src="images/avatar.jpg" alt="">';

                                $profilePic = 'http://placehold.it/50x50&text=NO+PROFILE+PIC';
                                $avatar = MemberPhotos::findFirst('member_id = "'.$whatsupTopics->member_id.'" and primary_pic="Yes"');
                                if ($avatar) {
                                    $profilePic =  'http://mybarangay.com'.$this->url->getBaseUri() . $avatar->file_path . $avatar->filename;
                                }
                                $hm .= '<a class="restricted" href="<?=$this->url->getBaseUri()?>member/page/<?=$whatsupTopics->member_id?>">';
                                $hm .= Phalcon\Tag::image(array($profilePic,  'style' => 'width:50px;height:50px'));
                                $hm .= '</a>';


                        $hm .= '</div>';//end of avatar

                        // $hm .= '<div class="icons">';//start of icons
                        //     $hm .= '<img src="images/icon1.jpg" alt=""><img src="images/icon4.jpg" alt=""><img src="images/icon5.jpg" alt=""><img src="images/icon6.jpg" alt="">';
                        // $hm .= '</div>';//end of icons
                    $hm .= '</div>';//end of userinfo
                    //------------------------------------------------------------

                    //----------------------------------------------------------
                    $hm .= '<div class="posttext pull-left">';//start of posttext
                        $hm .= '<h2>'.$whatsupTopics->subject.'</h2>';
                        $hm .= '<p>'.$whatsupTopics->content.'</p>';
                    $hm .= '</div>';//end of posttext
                    //----------------------------------------------------------

                    $hm .= '<div class="clearfix"></div>';

                $hm .= '</div>';//end of topwrap

                //----------------------------------------------------------
                $hm .= '<div class="postinfobot">';//start of topwrap

                    // $hm .= '<div class="likeblock pull-left">';//start of likeblock
                    //     $hm .= '<a href="#" class="up"><i class="fa fa-thumbs-o-up"></i>25</a>';
                    //     $hm .= '<a href="#" class="down"><i class="fa fa-thumbs-o-down"></i>3</a>';
                    // $hm .= '</div>';//end of likeblock

                    // $hm .= '<div class="prev pull-left"> ';//end of prev                                 
                    //     $hm .= '<a href="#"><i class="fa fa-reply"></i></a>';
                    // $hm .= '</div>';//end of prev

                    $hm .= '<div class="posted pull-left"><i class="fa fa-clock-o"></i> Posted on : '.$whatsupTopics->created_at.'</div>';

                    $hm .= '<div class="next pull-right">';//end of next pull                                          
                        // $hm .= '<a href="#"><i class="fa fa-share"></i></a>';
                        $hm .= '<a href="#"><i class="fa fa-flag"></i></a>';
                    $hm .= '</div>';//end of next pull  

                    $hm .= '<div class="clearfix"></div>';
                $hm .= '</div>';//end of postinfobot
                //----------------------------------------------------------

            $hm .= '</div>';//end of beforepagination

            $ccount = 1; foreach ($topicReplies as  $replies) {
            //----------------------------------------------------------
            $hm .= '<div class="post " style="width:95%;margin-left:5%">';
                $hm .= '<div class="topwrap">';
                    $hm .= '<div class="userinfo pull-left">';
                        $hm .= '<div class="avatar">';
                                $profilePic = 'http://placehold.it/50x50&text=NO+PROFILE+PIC';
                                $avatar = MemberPhotos::findFirst('member_id = "'.$replies->member_id.'" and primary_pic="Yes"');
                                if ($avatar) {
                                    $profilePic =  'http://mybarangay.com'.$this->url->getBaseUri() . $avatar->file_path . $avatar->filename;
                                }
                                $hm .= '<a class="restricted" href="<?=$this->url->getBaseUri()?>member/page/<?=$replies->member_id?>">';
                                $hm .= Phalcon\Tag::image(array($profilePic,  'style' => 'width:50px;height:50px'));
                                $hm .= '</a>';
                        $hm .= '</div>';

                        $hm .= '<div class="icons">';
                            //$hm .= '<img src="images/icon3.jpg" alt=""><img src="images/icon4.jpg" alt=""><img src="images/icon5.jpg" alt=""><img src="images/icon6.jpg" alt="">';
                        $hm .= '</div>';
                    $hm .= '</div>';
                    $hm .= '<div class="posttext pull-left">';
                        $hm .= '<p>'.$replies->content.'</p>';
                    $hm .= '</div>';
                    $hm .= '<div class="clearfix"></div>';
                $hm .= '</div>';       
                $hm .= '<div class="postinfobot">';

                    $hm .= '<div class="likeblock pull-left">';
                        // $hm .= '<a href="#" class="up"><i class="fa fa-thumbs-o-up"></i>10</a>';
                        // $hm .= '<a href="#" class="down"><i class="fa fa-thumbs-o-down"></i>1</a>';
                    $hm .= '</div>';

                    $hm .= '<div class="prev pull-left">';                               
                        // $hm .= '<a href="#"><i class="fa fa-reply"></i></a>';
                    $hm .= '</div>';

                    $hm .= '<div class="posted pull-left"><i class="fa fa-clock-o"></i> Posted on : '.$replies->created_at.'</div>';

                    $hm .= '<div class="next pull-right">';                    
                        // $hm .= '<a href="#"><i class="fa fa-share"></i></a>';
                        $hm .= '<a href="#"><i class="fa fa-flag"></i></a>';
                    $hm .= '</div>';

                    $hm .= '<div class="clearfix"></div>';
                $hm .= '</div>';
            $hm .= '</div>';
            //----------------------------------------------------------
            }

            $hm .= '<div id="replyAjax"></div>';

           $hm .= '<div class="post " style="width:95%;margin-left:5%">';
            $hm .= '<form method="post" id="postReply">';
                     $hm .= '<div class="topwrap">';
                         $hm .= '<div class="userinfo pull-left">';
                             $hm .= '<div class="avatar">';
                                    if(isset($userSession['id'])){
                                        // profile picture 
                                        $profilePic = 'http://placehold.it/50x50&text=NO+PROFILE+PIC';
                                        $avatar = MemberPhotos::findFirst('member_id = "'.$whatsupTopics->member_id.'" and primary_pic="Yes"');
                                        if ($avatar) {
                                        $profilePic =  'http://mybarangay.com'.$this->url->getBaseUri() . $avatar->file_path . $avatar->filename;
                                        }
                                        $hm .= '<a class="restricted" href="'.$this->url->getBaseUri().'member/page/'.$whatsupTopics->member_id;
                                           $hm .= Phalcon\Tag::image(array($profilePic,  'style' => 'width:50px;height:50px')); 
                                        $hm .= '</a>';
                                    }else{
                                        $hm .= '<img src="http://placehold.it/50x50&text=NO+PROFILE+PIC" alt="">';
                                    }


                                 
                                 $hm .= '<div class="status red">&nbsp;</div>';
                             $hm .= '</div>';

                             $hm .= '<div class="icons">';
                                 $hm .= '<img src="images/icon3.jpg" alt=""><img src="images/icon4.jpg" alt=""><img src="images/icon5.jpg" alt=""><img src="images/icon6.jpg" alt="">';
                             $hm .= '</div>';
                         $hm .= '</div>';
                         $hm .= '<div class="posttext pull-left">';

                            $hm .= '<div class="textwraper">';

                                if(isset($userSession['id'])){
                                 $hm .= '<div class="postreply">Post a Reply</div>';
                                  $hm .= '<div class="editor-wrapper">';
                                  $hm .= '<input type="hidden" id="rep_topicid" name="topic_id" value="'.$id.'">';
                                      $hm .= '<textarea id="reply_content" name="content" placeholder="Content here ...." class="form-control"></textarea>';
                                  $hm .= '</div>';
                                 $hm .= '</div>';
                                }else{
                                    $hm .= '<div class="Note Closed SignInOrRegister">';
                                    $hm .= '<a class="restricted" href="#" data-toggle="modal" data-target="#login-modal">Login</a>';
                                    $hm .= 'or <a class="restricted" href="#" data-toggle="modal" data-target="#register-modal">Signup</a> to comment.';
                                    $hm .= '</div>';
                                }
                             $hm .= '</div>';
                         $hm .= '<div class="clearfix"></div>';
                     $hm .= '</div>';      


                if(isset($userSession['id'])){

                     $hm .= '<div class="postinfobot">';
                         $hm .= '<div class="notechbox pull-left">';
                          $hm .= '<!--    <input name="note" id="note" class="form-control" type="checkbox"> -->';
                         $hm .= '</div>';

                         $hm .= '<div class="pull-left">';
                            $hm .= '<!--  <label for="note"> Email me when some one post a reply</label> -->';
                         $hm .= '</div>';

                         $hm .= '<div class="pull-right postreply">';
                            $hm .= '<!--  <div class="pull-left smile"><a href="#"><i class="fa fa-smile-o"></i></a></div> -->';
                             $hm .= '<div class="pull-left"><button type="submit" class="btn btn-primary">Post Reply</button></div>';
                             $hm .= '<div class="clearfix"></div>';
                         $hm .= '</div>';
                }
                         $hm .= '<div class="clearfix"></div>';
                     $hm .= '</div>';

            $hm .= '</form>';
             $hm .= '</div><!-- POST -->';


            $output = json_encode(array('type'=>'ok', 'text' => $hm));
            die($output); //exit script outputting json data

        }

    }

    /**
     * [` description]
     */
    public function addTopicAction(){
        $userSession = $this->session->get("userSession"); 
        if (!isset($userSession['id']) or empty($userSession['id'])) {
                return false;
        }
        $this->view->disable();
        if($this->request->isAjax()) {
          $hm = '';
      $hm .= $this->tag->javascriptInclude("themes3/themes/ckeditor/ckeditor.js"); 
        $hm .= '<form method="post" action="" id="addTopic" class="newtopic form">';//start of topwrap
        // $hm .= Phalcon\Tag::form(array("#", "method" => "post", "id" => "addTopic", "class" => "form newtopic"));
        $hm .= '<div class="messages"></div>';
        $hm .= '<div class="post">'; //start of post
            $hm .= '<div class="topwrap">';//start of topwrap

                //-----------------------------------------------------------
                $hm .= '<div class="userinfo pull-left">';//start of userinfo

                    //-----------------------------------------------------------
                    $hm .= '<div class="avatar">';//start of avatar
                        $profilePic = 'http://placehold.it/50x50&text=NO+PROFILE+PIC';
                        if( !empty($userSession) ) { 
                            $avatar = MemberPhotos::findFirst('member_id = "'.$userSession['id'].'" and primary_pic="Yes"');
                            if ($avatar) {
                                $profilePic =  'http://mybarangay.com'.$this->url->getBaseUri() . $avatar->file_path . $avatar->filename;
                            }
                        }
                        $hm .= '<a class="restricted" href="<?=$this->url->getBaseUri()?>member/page/<?=$topic->member_id?>">';
                            $hm .= Phalcon\Tag::image(array($profilePic,  'style' => 'width:50px;height:50px')); 
                        $hm .= '</a>';
                        $hm .= '<div class="status red">&nbsp;</div>';
                    $hm .= '</div>';//end of avatar
                    //-----------------------------------------------------------

                    //-----------------------------------------------------------
                    $hm .= '<div class="icons">';//start of icons
                        $hm .= '<img src="images/icon3.jpg" alt=""><img src="images/icon4.jpg" alt=""><img src="images/icon5.jpg" alt=""><img src="images/icon6.jpg" alt="">';
                    $hm .= '</div>';//end of icons
                    //-----------------------------------------------------------

                $hm .= '</div>';//end of userinfo
                //-----------------------------------------------------------

                //-----------------------------------------------------------
                $hm .= '<div class="posttext pull-left">';//start of posttext
                  
                        $hm .= '<input name="subject" placeholder="Enter Topic Title" class="form-control" type="text">';
                

                    $hm .= '<div class="row">';//start of row
                        $hm .= '<div class="col-lg-12 col-md-12">';//start of row col-lg-12 col-md-12
                        $hm .= '<select name="whatsup_category_id" id="whatsup_category_id" class="form-control">';
                            $hm .= '<option value="" disabled="" selected="">Select Category</option>';
                            foreach (WhatsupCategories::find() as $value) {
                                $hm .= ' <option value="'.$value->id.'">'.$value->name.'</option>';
                            }
                        $hm .= '</select>';
                        $hm .= '</div>';//end of row col-lg-12 col-md-12
                    $hm .= '</div>';//end of row

                    $hm .= '<div class="editor-wrapper">';//start of editor-wrapper
                        $hm .= '<textarea  name="content" placeholder="Content here ...." class="ckeditor form-control"></textarea>';
                    $hm .= '</div>';//end of editor-wrapper
                $hm .= '</div>';//end of posttext
                //-----------------------------------------------------------

                $hm .= '<div class="clearfix"></div>';

            $hm .= '</div>';//end of topwrap  

            //-----------------------------------------------------------
            $hm .= '<div class="postinfobot">';//start of postinfobot 
                $hm .= '<div class="pull-right postreply">';//start of postreply 
                    //$hm .= '<div class="pull-left smile"><a href="#"><i class="fa fa-smile-o"></i></a></div>';
                    $hm .= '<div class="pull-left"><button type="submit " style="margin-right:5px" class="btn btn-grey btn-sm">publish</button>';
                    $hm .= '<button type="reset" class="btn btn-grey btn-sm">reset</button></div>';
                    $hm .= '<div class="clearfix"></div>';
                $hm .= '</div>';//end of postreply 
            $hm .= '<div class="clearfix"></div>';
            $hm .= '</div>';//end of postinfobot  
            //-----------------------------------------------------------
        $hm .= '</div>';// end of post
        $hm .= Phalcon\Tag::endForm();

        $output = json_encode(array('type'=>'ok', 'text' => $hm));
        die($output); //exit script outputting json data

        }
    }

    /**
     * [addAction description]
     */
    public function addAction(){
      $userSession = $this->session->get("userSession"); 
      if($this->request->isAjax()) {
         $this->view->disable();

        $msg = '';
        $topicid = '';
        $hm = '';
        $res = '';
        $subject = $this->request->getQuery("subject");
        $category = $this->request->getQuery("category");
        $content = $this->request->getQuery("content");

        $wtsTopic = new WhatsupTopics();
        $wtsTopic->subject             =  $subject;
        $wtsTopic->created_at          =  date('Y-m-d H:i:s');
        $wtsTopic->member_id           =  $userSession['id'];
        $wtsTopic->whatsup_category_id =  $category;
        $wtsTopic->content             =  $content;
        if ($wtsTopic->create()) {
          $this->view->linkfx = 'z_lat';
          $res = 'success';
        }else{
          $res = 'error';
        }
        $output = json_encode(array('type'=> $res, 'message' => $msg, 'id' => $topicid));
        die($output); //exit script outputting json data
      }

        // $this->view->disable();
        // if($this->request->isAjax()) {
        //     $msg = '';
        //     $topicid = '';
        //     $hm = '';
        //      $userSes = $this->session->get("userSession");
        //      parse_str($this->request->getQuery('serial'), $_ZOID);

        //         $wtsTopic = new WhatsupTopics();
        //         $wtsTopic->subject             =  (!empty($_ZOID['subject']))? $_ZOID['subject'] : NULL ;
        //         $wtsTopic->created_at          =  date('Y-m-d H:i:s');;
        //         $wtsTopic->member_id           =  $userSes['id'];
        //         $wtsTopic->whatsup_category_id =  (!empty($_ZOID['whatsup_category_id']))? $_ZOID['whatsup_category_id'] : NULL ;
        //         $wtsTopic->content             =  (!empty($_ZOID['content']))? $_ZOID['content'] : NULL ;
        //         if ($wtsTopic->create()) {
                 
        //             $response = 'success';
        //             $topicid = $wtsTopic->id;

        //             $msg .= '<div class="alert alert-success fade in">';
        //                 $msg .= '<a href="#" class="close" data-dismiss="alert">&times;</a>';
        //                 $msg .= '<strong>Success!</strong> New Topic successfuly created';
        //             $msg .= '</div>';


        //         }else{
        //             $response = 'error';
   
        //             $msg .= '<div class="alert alert-danger fade in">';
        //                 $msg .= '<a href="#" class="close" data-dismiss="alert">&times;</a>';
        //                 $msg .= '<strong>Error! </strong> ';
        //                 // foreach ($wtsTopic->getMessages() as  $er) {
        //                 //     $msg .= $er->getMessage() .'</br>';
        //                 // }
        //                 $msg .= 'all fields are required';
        //             $msg .= '</div>';
        //         }
             
        //     $output = json_encode(array('type'=> $response, 'message' => $msg, 'text' => $hm, 'id' => $topicid));
        //     die($output); //exit script outputting json data
        // }

    }//end of function addAction

    /**
     * [searchAction description]
     * @return [type] [description]
     */
    public function searchAction(){

        $this->view->disable();
        if($this->request->isAjax()) {

            $key = '';
            $hm = '';
            $sort = 'desc';
            //$catg = $this->request->getQuery('catg');
            $sort = $this->request->getQuery('sort');
            $view = $this->request->getQuery('view');

            $key = $this->request->getQuery('keyword');
            $wtsTopic = WhatsupTopics::find([
                'conditions' => 'subject LIKE :key:',
                'bind' => ['key' => '%'.$key.'%'],
                'limit' => 10,
                'order' => 'id '.$sort
            ]);

            $setup = array('sort' => $sort, 'view' => $view, 'search' => $key );
            return $this->postModerator($wtsTopic, $catg = '', $setup);
            // $output = json_encode(array('type'=> 'ok', 'text' => $hm));
            // die($output); //exit script outputting json data
        }

    }




    /**
     * [searchAction description]
     * @return [type] [description]
     */
    public function latestAction(){
        $this->view->disable();
        if($this->request->isAjax()) {
            $hm = '';
            $sort = 'desc';
            $catg = $this->request->getQuery('catg');
            $sort = $this->request->getQuery('sort');
            $view = $this->request->getQuery('view');

            $today = date('Y-m-d', strtotime("-30 days"));
            $que = "SELECT * FROM WhatsupTopics WHERE '$today' <= created_at ORDER BY id DESC "; 
            $query = $this->modelsManager->createQuery($que);
            $latest = $query->execute();
            $setup = array('sort' => $sort, 'view' => $view);
            return $this->postModerator($latest, $catg = '', $setup);
        }
    }


    public function editTopicAction(){
      $this->view->disable();
      if($this->request->isAjax()) {
          $html = '';
          $sort = 'desc';
          $catg = $this->request->getQuery('catg');
          $sort = $this->request->getQuery('sort');
          $view = $this->request->getQuery('view');
          $userSession = $this->session->get("userSession"); 
          $member_id = $userSession['id'];

           $id = $this->request->getQuery('id');
           $this->persistent->view_id = $id;
           $whatsupTopics = WhatsupTopics::findFirstById($id); 

          $hm = '';
      $hm .= $this->tag->javascriptInclude("themes3/themes/ckeditor/ckeditor.js"); 
        $hm .= '<form method="post" action="" id="addTopic" class="newtopic form">';//start of topwrap
        // $hm .= Phalcon\Tag::form(array("#", "method" => "post", "id" => "addTopic", "class" => "form newtopic"));
        $hm .= '<div class="messages"></div>';
        $hm .= '<div class="post">'; //start of post
            $hm .= '<div class="topwrap">';//start of topwrap

                //-----------------------------------------------------------
                $hm .= '<div class="userinfo pull-left">';//start of userinfo

                    //-----------------------------------------------------------
                    $hm .= '<div class="avatar">';//start of avatar
                        $profilePic = 'http://placehold.it/50x50&text=NO+PROFILE+PIC';
                        if( !empty($userSession) ) { 
                            $avatar = MemberPhotos::findFirst('member_id = "'.$userSession['id'].'" and primary_pic="Yes"');
                            if ($avatar) {
                                $profilePic =  'http://mybarangay.com'.$this->url->getBaseUri() . $avatar->file_path . $avatar->filename;
                            }
                        }
                        $hm .= '<a class="restricted" href="<?=$this->url->getBaseUri()?>member/page/<?=$topic->member_id?>">';
                            $hm .= Phalcon\Tag::image(array($profilePic,  'style' => 'width:50px;height:50px')); 
                        $hm .= '</a>';
                        $hm .= '<div class="status red">&nbsp;</div>';
                    $hm .= '</div>';//end of avatar
                    //-----------------------------------------------------------

                $hm .= '</div>';//end of userinfo
                //-----------------------------------------------------------



                //-----------------------------------------------------------
                $hm .= '<div class="posttext pull-left">';//start of posttext
                  
                        $hm .= '<input name="subject" placeholder="Enter Topic Title" class="form-control" type="text" value="'.$whatsupTopics->subject.'">';
                

                    $hm .= '<div class="row">';//start of row
                        $hm .= '<div class="col-lg-12 col-md-12">';//start of row col-lg-12 col-md-12
                      $hm .= '<select name="whatsup_category_id" id="whatsup_category_id" class="form-control">';
                      $hm .= '<option value="">Select Country</option>';
                      $categories = WhatsupCategories::find();
                          foreach ($categories as $categ) {
                         if ($categ->id == $whatsupTopics->whatsup_category_id) {
                            $hm .= '<option value="'.$categ->id.'" selected>'.$categ->name.'</option>';
                          } else {
                            $hm .= '<option value="'.$categ->id.'">'.$categ->name.'</option>';
                        }
                       }
                      $hm .= '</select>';



                        $hm .= '</div>';//end of row col-lg-12 col-md-12
                    $hm .= '</div>';//end of row

                    $hm .= '<div class="editor-wrapper">';//start of editor-wrapper
                        $hm .= '<textarea  name="content" placeholder="Content here ...." class="ckeditor form-control">'.$whatsupTopics->content.'</textarea>';
                    $hm .= '</div>';//end of editor-wrapper
                $hm .= '</div>';//end of posttext
                //-----------------------------------------------------------

                $hm .= '<div class="clearfix"></div>';

            $hm .= '</div>';//end of topwrap  

            //-----------------------------------------------------------
            $hm .= '<div class="postinfobot">';//start of postinfobot 
                $hm .= '<div class="pull-right postreply">';//start of postreply 
                    //$hm .= '<div class="pull-left smile"><a href="#"><i class="fa fa-smile-o"></i></a></div>';
                    $hm .= '<div class="pull-left"><button type="submit " style="margin-right:5px" class="btn btn-grey btn-sm">publish</button>';
                    $hm .= '<button type="reset" class="btn btn-grey btn-sm">reset</button></div>';
                    $hm .= '<div class="clearfix"></div>';
                $hm .= '</div>';//end of postreply 
            $hm .= '<div class="clearfix"></div>';
            $hm .= '</div>';//end of postinfobot  
            //-----------------------------------------------------------
        $hm .= '</div>';// end of post
        $hm .= Phalcon\Tag::endForm();


        $output = json_encode(array('type'=> 'success', 'text' => $hm));
        die($output); //exit script outputting json data





        }
    }




    public function popularAction(){
        $this->view->disable();
        if($this->request->isAjax()) {
            $html = '';
            $sort = 'desc';
            $catg = $this->request->getQuery('catg');
            $sort = $this->request->getQuery('sort');
            $view = $this->request->getQuery('view');

          $popular = $this
          ->modelsManager
          ->createBuilder()
          ->from(array('wt' => 'WhatsupTopics'))
          ->join('WhatsupReplies', "wp.whatsup_topic_id = wt.id", 'wp')
          ->columns(array('count(wp.id) as sample', 'wp.content',  'wt.id','wt.subject','wt.created_at','wt.member_id','wt.whatsup_category_id'))
          ->groupBy('wt.subject')
          ->orderBy('count(wp.id) desc')
          ->limit(20)
          ->getQuery()
          ->execute();
            $setup = array('sort' => $sort, 'view' => $view);
            return $this->postModerator($popular, $catg, $setup);

        }
    }

    /**
     * [replyAction description]
     * @return [type] [description]
     */
    public function replyAction(){
        $this->view->disable();
        if($this->request->isAjax()) {
            $userSes = $this->session->get("userSession");
            $msg = '';
            $hm = '';
            $topic_id = '';
            $content = '';
            $response = '';
            $content = $this->request->getQuery('content');
            $topic_id = $this->request->getQuery('id');
 
            $replies = new WhatsupReplies();
            $replies->content = $content;
            $replies->created_at = date('Y-m-d H:i:s');
            $replies->modified_at = date('Y-m-d H:i:s');
            $replies->whatsup_topic_id = $topic_id;
            $replies->member_id = $userSes['id'];
            // if true create reply
            if($replies->create()){
                $response = 'success';
                $msg .= '<div class="alert alert-success fade in">';
                    $msg .= '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                    $msg .= '<strong>Success! </strong> ';
                    $msg .= 'Failed posting reply';
                $msg .= '</div>';

            //----------------------------------------------------------
            $hm .= '<div class="post"  style="width:95%;margin-left:5%">';
                $hm .= '<div class="topwrap" id="postr'.$replies->id.'">';
                    $hm .= '<div class="userinfo pull-left" style="width:10%">';
                        $hm .= '<div class="avatar">';
                                $profilePic = 'http://placehold.it/50x50&text=NO+PROFILE+PIC';
                                $avatar = MemberPhotos::findFirst('member_id = "'.$replies->member_id.'" and primary_pic="Yes"');
                                if ($avatar) {
                                    $profilePic =  'http://mybarangay.com'.$this->url->getBaseUri() . $avatar->file_path . $avatar->filename;
                                }
                                $hm .= '<a class="restricted" href="<?=$this->url->getBaseUri()?>member/page/<?=$replies->member_id?>">';
                                $hm .= Phalcon\Tag::image(array($profilePic,  'style' => 'width:30px;height:30px'));
                                $hm .= '</a>';
                        $hm .= '</div>';

                        $hm .= '<div class="icons">';
                            //$hm .= '<img src="images/icon3.jpg" alt=""><img src="images/icon4.jpg" alt=""><img src="images/icon5.jpg" alt=""><img src="images/icon6.jpg" alt="">';
                        $hm .= '</div>';
                    $hm .= '</div>';
                    $hm .= '<div class="posttext pull-left">';
                        $hm .= '<p>'.$replies->content.'</p>';
                    $hm .= '</div>';
                      if($userSes['id'] == $replies->member_id){
                        $hm .= ' <a href="#" class="close closex" id="'.$replies->id.'"  style="background:#043081; border-radius:60px;padding:2px;margin:5px;">&times;</a>';
                      }
                    $hm .= '<div class="clearfix"></div>';
                $hm .= '</div>';       
                $hm .= '<div class="postinfobot" style="padding:0px;margin:0px;line-height:30px">';

                    $hm .= '<div class="likeblock pull-left">';
                        // $hm .= '<a href="#" class="up"><i class="fa fa-thumbs-o-up"></i>10</a>';
                        // $hm .= '<a href="#" class="down"><i class="fa fa-thumbs-o-down"></i>1</a>';
                    $hm .= '</div>';

                    $hm .= '<div class="prev pull-left">';                               
                        // $hm .= '<a href="#"><i class="fa fa-reply"></i></a>';
                    $hm .= '</div>';

                    $hm .= '<div class="posted pull-left"><i class="fa fa-clock-o"></i> Posted on : '.$replies->created_at.'</div>';

                    $hm .= '<div class="clearfix"></div>';
                $hm .= '</div>';
            $hm .= '</div>';
            //----------------------------------------------------------

            }else{

                $response = 'error';
                $msg .= '<div class="alert alert-danger fade in">';
                    $msg .= '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                    $msg .= '<strong>Error! </strong> ';
                    $msg .= 'Failed posting reply';
                    foreach ($replies->getMessages() as  $err) {
                     $msg .= $err->getMessage().'</br>';
                    }
                $msg .= '</div>';
            }
            
            $output = json_encode(array('type'=> $response, 'text' => $hm, 'message' => $msg));
            die($output); //exit script outputting json data
        }
    }

   /**
     * [flagAction description]
     * @return [type] [description]
     */
    public function flagAction(){

        $this->view->disable();

        if($this->request->isAjax()){

            $review = $this->request->getQuery('review');
            $arrayName = array('1','2');
            $html .= json_encode(array('result' => 'OK', 'items' => $arrayName));

        }

    }

    public function topicListAction(){

    }


}
