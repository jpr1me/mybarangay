<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Google\Client;
use Google\Service\Plus;
use Google\Http\Request;
use Google\IO\Curl;
use \Phalcon\Tag;


class BizController extends ControllerBase
{

  public function initialize()
  {
    parent::initialize();
  }


  public function signupAction($business_id = null)
    {
    $this->view->setTemplateAfter('oneheader_default');
    $userSession = $this->session->get('userSession');

    if ($business_id == null) {
      // do nothing
    } else {
      $this->view->setVar('business_id' , $business_id);
    }

    if ($userSession['type'] == 'Business') {
      return $this->response->redirect('biz/page/'. $userSession['id']);
    } else if ($userSession['type'] == 'Member') {
    	$this->session->destroy();
	      $date_of_expiry = time() - 60 ;
	      setcookie( "mid", "anonymous", $date_of_expiry, "/");
	      setcookie( "e", "anonymous", $date_of_expiry, "/");
	      setcookie( "token", "anonymous", $date_of_expiry, "/");
	      $this->view->setVar('userSession', '');
    }

      if ($this->request->isPost()) {
      $error = 0;
      // if($this->security->checkToken() == false){
      //  $error = 1;
      //  $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Invalid CSRF Token');
      //  return $this->response->redirect('signup');
      // }
      $firstName = $this->request->getPost('first_name');
      $lastName = $this->request->getPost('last_name');
      $email = $this->request->getPost('email'); 
      $password = $this->request->getPost('password');
      $confirmPassword = $this->request->getPost('confirm_password');
      $business_id = $this->request->getPost('biz_id');
      
      $oldInputs = array();
      $oldInputs['first_name'] = $this->request->getPost('first_name');
      $oldInputs['last_name'] = $this->request->getPost('last_name');
      $oldInputs['email'] = $this->request->getPost('email'); 
      $oldInputs['password'] = $this->request->getPost('password');
      $oldInputs['confirm_password'] = $this->request->getPost('confirm_password');

      if(empty($firstName) || empty($lastName)  || empty($email)  || empty($password)  || empty($confirmPassword)){ 
        $this->flash->warning('<button type="button" class="close" data-dismiss="alert">×</button>All fields required');
        return $this->response->redirect(); 
      }

      if($password != $confirmPassword){ 
        $errorMsg = "Confirm password does not match"; 
        $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>'.$errorMsg);        
        return $this->response->redirect();
      }


      if(!empty($email) && Members::findFirstByEmail($email)){
        //$errorMsg = "Email address is already in use here or as a MyBarangay Member. Please use another email address.";
        $this->session->set("oldInputs", $oldInputs);
        $this->session->set("signup-biz", "Email address is already in use here or as a MyBarangay Member. Please use another email address.");        
        //$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>'.$errorMsg);
        return $this->response->redirect('biz/signup');
      }

      $member = new Members();
      $member->created = date('Y-m-d H:i:s');
      $member->modified = date('Y-m-d H:i:s');
      $member->first_name = $firstName;
      $member->last_name = $lastName;
      $member->email = $email;
      $member->type = 'Business';
      $member->password = $this->security->hash($password);
      
      if($member->create()){
        $activationToken = substr(str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ), 0, 50);
        $emailConfimation = new EmailConfirmations();
        $emailConfimation->created = date('Y-m-d H:i:s');
        $emailConfimation->modified = date('Y-m-d H:i:s');
        $emailConfimation->user_id = $member->id;
        $emailConfimation->email = $email;
        $emailConfimation->token = $activationToken;
        $emailConfimation->business_id = $business_id;
        $emailConfimation->claim_status = '0';
        $emailConfimation->confirmed = 'N';
        if($emailConfimation->save()){
          $this->getDI()->getMail()->send(
                        array($email => $firstName.' '.$lastName),
                        'Please confirm your email',
                        'confirmation',
                        array( 'confirmUrl' => 'biz/emailConfimation/'. $member->id .'/'. $email .'/'. $activationToken)
                    );
        }
        $this->session->set("signup-biz", "You've successfully created a MyBarangay for Business account but you need to verify it. Please check your email to confirm it.");
        //$this->flash->success('<button type="button" cl ass="close" data-dismiss="alert">×</button>You\'ve successfully created a MyBarangay account. We sent a confirmation email to '.$email.'.');
        $this->response->redirect('');
      } else {
        //print_r($user->getMessages());
        $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Registration failed. Please try again.');
        $this->response->redirect('biz/signup');
      }
      return $this->response->redirect('biz/signup/'.$business_id);
    } 

    }

    public function emailConfimationAction($userId = null, $email =null, $activationToken = null){
      $emaiConfirmed = EmailConfirmations::findFirst(array('columns'    => '*', 
                                 'conditions' => 'user_id = ?1 AND email=?2 AND token = ?3 AND confirmed = ?4', 
                                 'bind' => array(1 => $userId, 2 => $email, 3 => $activationToken, 4 => 'N')));
      if($emaiConfirmed) {
          $emaiConfirmed->confirmed = 'Y';
          $emaiConfirmed->update();
          $this->session->set("loginAsAMember", "Your email was successfully confirmed.");
          //$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button><H4>You \'re email has been confirmed.</H4>You\'re now officially part of the <strong>MyBarangay</strong> community. Mabuhay!');
          return $this->response->redirect('biz/claimreq_success/'.$emaiConfirmed->user_id.'/'.$emaiConfirmed->business_id);
      } else {
        return $this->response->redirect('biz/signup');
      }
    }


    /**
     * Login user
     * @return \Phalcon\Http\ResponseInterface
     */
    public function loginAction()
    {
    $this->view->setTemplateAfter('oneheader_default');
  $userSession = $this->session->get('userSession');
   if (isset($userSession['type'])) {
      if ($userSession['type'] == 'Member') {
    $this->session->set("loginAsAMember", "You will be logged out as Basic Member in MyBarangay before you can login with your Business Owner account.");
      }
   }
        if ($this->request->isPost()) {
   if (isset($userSession['type'])) {
      if ($userSession['type'] == 'Member') {
    $this->session->set("loginAsAMember", "This email address is already used by another Member in MyBarangay, Please Register or Login as a Business Owner");
      return $this->response->redirect('biz/login');
      }
   }
      // if($this->security->checkToken() == false){
      //  $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Invalid CSRF Token');
      //  return $this->response->redirect('login');
      // }
      $this->view->disable();
      $email = $this->request->getPost('email'); // $_POST
      $password = $this->request->getPost('password');

      if(empty($email) || empty($password)){ 
        $this->flash->warning('<button type="button" class="close" data-dismiss="alert">×</button>All fields required');
        return $this->response->redirect(''); 
      }

      $member = Members::findFirstByEmail($email);
      if ($member->type != 'Business') { return $this->response->redirect(); }
      if ($member == true && $this->security->checkHash($password, $member->password)) {

        $emaiConfirmed = EmailConfirmations::findFirst(array('columns'    => '*', 
                                 'conditions' => 'user_id = ?1 AND email=?2 AND confirmed = ?3', 
                                 'bind' => array(1 => $member->id, 2 => $email, 3 => 'Y')));

        if(!$emaiConfirmed) {
           $this->session->set("loginAsAMember", "Your email is not yet confirmed. Please check and confirm your email.");
          $this->flash->warning('<button type="button" class="close" data-dismiss="alert">×</button>You\'re email is not yet confirmed.');
          return $this->response->redirect('biz/login/');
        }

        $userSession = get_object_vars($member);
        //$userSession['type'] = 'Member';
        $profilePic = MemberPhotos::findFirst(array('member_id="'.$userSession['id'].'"', 'primary_pic="Yes"'));
        $userSession['primary_pic'] = $profilePic->file_path.$profilePic->filename;
        $this->session->set('userSession', $userSession);
        //member id
        $cookie_name = "mid";
        $cookie_value = $userSession['id'];
        $date_of_expiry = time() + 60 * 60 * 24 * 90;
        setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
        //email 
        $cookie_name = "e";
        $cookie_value = $userSession['email'];
        setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
        //cookie token
        $cookie_name = "token";
        $cookie_token = substr(md5(uniqid(rand(), true)), 0, 20);
        setcookie($cookie_name, $this->encrypt($cookie_token), $date_of_expiry, "/"); 
        $member->modified = date('Y-m-d H:i:s');
        $member->cookie_token = $this->security->hash($cookie_token);
        if($member->update()){
    
          $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>You are now logged in.');
          $this->response->redirect('biz/page/'.$userSession['id']);
        }

      } else {
        $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Incorrect username or password.');
        $this->response->redirect('');
      }
    }
    
    }
    
    function encrypt($pure_string) {
    $encryption_key = 'sMeynBiaprpainlgiyhP';
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
    return $encrypted_string;
  }

  public function logoutAction() {
      $this->view->disable();
      $this->session->destroy();
      $date_of_expiry = time() - 60 ;
      setcookie( "mid", "anonymous", $date_of_expiry, "/");
      setcookie( "e", "anonymous", $date_of_expiry, "/");
      setcookie( "token", "anonymous", $date_of_expiry, "/");
      $this->view->setVar('userSession', '');
      $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>You are now logged out.');
      
      return $this->response->redirect();
    }

    public function page1Action($id = null) {
  $this->view->setTemplateAfter('default1');
      $userSession = $this->session->get('userSession');
  if ($userSession['type'] == 'Member') {
    $this->response->redirect('member/page/'. $userSession['id']);
  } 
      $member = Members::findFirstById($id);
      $this->view->setVar('member', $member);

      $reviews = Reviews::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('reviews', $reviews);

      $claimRequests = ClaimRequests::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('claimrequest', $claimRequests);
    }

    public function pageAction($id = null) {
  $this->view->setTemplateAfter('default1');
      $userSession = $this->session->get('userSession');
  if ($userSession['type'] == 'Member') {
    $this->response->redirect('member/page/'. $userSession['id']);
  } 
      $member = Members::findFirstById($id);
      $this->view->setVar('member', $member);

      $reviews = Reviews::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('reviews', $reviews);

      $claimRequests = ClaimRequests::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('claimrequest', $claimRequests);
    }

    public function business_searchAction() {
      $searchWords = '';
        $business = array();
        if(isset($_GET["page"])){
            $currentPage = (int) $_GET["page"];
        } else {
            $currentPage = 1;
        }
        if ($this->request->isPost()) {
            $businessName = $this->request->getPost('name');
            $businessAddress = $this->request->getPost('address');
            $businessCategoryId = $this->request->getPost('business_category_id');

            $country = Countries::findFirst(array('columns'    => '*', 
                                             'conditions' => 'country LIKE :country:', 
                                             'bind' => array('country' => $businessAddress)));
          
            $countryId = '';
            if($country) {
                $countryId = $country->id;
            } 

            $businessCategoryLists = BusinessCategoryLists::find(array('columns'    => '*', 
                                             'conditions' => 'business_category_id = :business_category_id:',
                                             'bind' => array('business_category_id' => $businessCategoryId)));
            $conditions = '';
            if(!empty($businessCategoryLists)) {
                foreach ($businessCategoryLists as $key => $businessCategoryList) {
                    $conditions .= ' OR id = :'.$key.':';
                    $bind[$key] = $businessCategoryList->business_id;
                }//$searchWords .= ', '.$businessName;
            }

            if(!empty($businessName)) {
                $conditions .= ' OR name LIKE :name:';
                $bind['name'] = '%'.$businessName.'%';
                $searchWords .= ', '.$businessName;
            }

            if(!empty($businessAddress)) {
                $conditions .= ' OR street LIKE :street: OR city LIKE :city:';
                $bind['street'] = '%'.$businessAddress.'%';
                $bind['city'] = '%'.$businessAddress.'%';
                $searchWords .= ', '.$businessAddress;
            }

            if(!empty($countryId)) {
                $conditions .= ' OR country_id = :country_id:';
                $bind['country_id'] = $countryId;
                $searchWords .= ', '.$country->country;
            }

            $searchWords = substr($searchWords, 2); 
            $business = Business::find(array('columns'    => '*', 
                                     'conditions' => substr($conditions, 3), 
                                     'bind' => $bind
                                        ));

            $this->view->setVar('business', $business);
        } else {
            $business = Business::find(array('order'    => 'id DESC'));
        }

        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $business,
                "limit"=> 12,
                "page" => $currentPage
            )
        );
        $page = $paginator->getPaginate();
        $this->view->setVar('business', $page);
        //$this->view->setVar('business', $business);
        $businessCategories = BusinessCategories::find();
        $this->view->setVar('businessCategories', $businessCategories);
    }

    public function claimAction($business_id = null) {

      $userSession = $this->session->get("userSession");

      $type = $userSession['type'];
      $id   = $userSession['id'];
      $business = $business_id;
      if ($type != 'Business') {
        //$this->session->set("claimAsAUser", "Basic User account is already logged-in, Please Log it out and Register or Log-in as a Business Owner.");
        $this->session->set("proceed", $business_id);
        if ($type == null) {
            $g = '0';
        } else {
            $g = '1';
        }
        $this->session->set("guestoruser", $g);
        return $this->response->redirect($this->request->getHTTPReferer());
      } 
      //else {
        // return $this->response->redirect('biz/claimreq_success/'.$userSession['id'].'/'.$business_id);
      //}
    }

    public function respondAction($review_id = null, $business_id = null) {
      $userSession = $this->session->get("userSession");

      if ($this->request->isPost()) {
        $biz_id = $this->request->getPost("business_id");
        $respond = new ReviewResponds();
        $respond->created = date('Y-m-d H:i:s');
        $respond->modified = date('Y-m-d H:i:s');
        $respond->member_id = $userSession['id'];
        $respond->review_id = $this->request->getPost("review_id");
        $respond->business_id = $this->request->getPost("business_id");
        $respond->content = $this->request->getPost("content");

        if ($respond->create()) {
          return $this->response->redirect('business/view2/'.$biz_id);
        } else {
          $this->view->disable();
          print_r($respond->getMessages());
        }
      }

        $business = Business::findFirstById($business_id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
        $this->view->setVar('business', $business);
        $reviews = Reviews::findById($review_id);
        if (!$reviews) {
          $this->view->disable();
          echo "failed";
        }
        $this->view->setVar('reviews', $reviews);


    }
  
  public function claimreq_successAction($userId = null, $businessId = null) {
      //$this->view->setTemplateAfter('default1');
      $this->view->setTemplateAfter('oneheader_default');
      $business = Business::findFirst("id='".$businessId."'");
      $this->view->setVars([
        'userId' => $userId, 
        'businessId' => $businessId,
        'business' => $business
      ]);
  }    
  public function checkclaimAction($businessId = null) {
  if ($this->request->isPost()) {
    $referer = $this->request->getHTTPReferer();
    $firstNum = $this->request->getPost('biz_num');
    $alternateNum = $this->request->getPost('alt_num');
    $userId = $this->request->getPost('user_id');
    $businessId = $this->request->getPost("biz_id");
    if ($firstNum == '' && $alternateNum == '') {
    $this->session->set('failedtoconfirm', 1);
    return $this->response->redirect($referer);
    } else if ($firstNum == '' && $alternateNum != ''){
        $claimRequest = new ClaimRequests();
        $claimRequest->created = date('Y-m-d H:i:s');
        $claimRequest->modified = date('Y-m-d H:i:s');
        $claimRequest->member_id = $userId;
        $claimRequest->business_id = $businessId;
        $claimRequest->status = 'pending'; // default status
    $businessCredential = Business::findFirst("id='".$businessId."'");
    $businessAddress = $businessCredential->street .", ". $businessCredential->city.", ".$businessCredential->state.", ".$businessCredential->country_code_id;
    $userCredential = Members::findFirst("id=".$userId);
    if ($claimRequest->create()) {

    $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Business Claim has been sent.');
    $email = 'jopper_jan@yahoo.co.jp';
    $getBusinessName = Business::findFirst('id='.$businessId);
    $businessName = $getBusinessName->name;
    $this->getDI()->getMail()->send(
                        array($email => ''),
                        'Check on Business Establishment Claim',
                        'claim_req',
                        array( 'first_name' => $userCredential->first_name, 'last_name' => $userCredential->last_name, 'email'=> $userCredential->email, 'address' => $businessAddress, 'business_name' => $businessName, "telephone" =>$alternateNum)
                );
      //return $this->response->redirect('business/view/'. $business_id);
      $this->session->set("claimingsuccess", 1);
          return $this->response->redirect('business/view2/'.$businessId);
        } else {
          $this->view->disable();
          print_r($claimRequest->getMessages());
        }
    //$this->session->set("claimingsuccess", 1);
    return $this->response->redirect('business/view2/'.$businessId);
    } else if ($firstNum != '' && $alternateNum == ''){
    $claimRequest = new ClaimRequests();
        $claimRequest->created = date('Y-m-d H:i:s');
        $claimRequest->modified = date('Y-m-d H:i:s');
        $claimRequest->member_id = $userId;
        $claimRequest->business_id = $businessId;
        $claimRequest->status = 'pending'; // default status
    $businessCredential = Business::findFirst("id='".$businessId."'");
    $businessAddress = $businessCredential->street .", ". $businessCredential->city.", ".$businessCredential->state.", ".$businessCredential->country_code_id;
    $userCredential = Members::findFirst("id=".$userId);
        if ($claimRequest->create()) {
    $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Business Claim has been sent.');
    $email = 'jopper_jan@yahoo.co.jp';
    $getBusinessName = Business::findFirst('id='.$businessId);
    $businessName = $getBusinessName->name;
    $this->getDI()->getMail()->send(
                        array($email => ''),
                        'Check on Business Establishment Claim',
                        'claim_req',
                        array( 'first_name' => $userCredential->first_name, 'last_name' => $userCredential->last_name, 'email'=> $userCredential->email, 'address' => $businessAddress, 'business_name' => $businessName, "telephone" =>$firstNum)
                );
      //return $this->response->redirect('business/view/'. $business_id);
      $this->session->set("claimingsuccess", 1);
          return $this->response->redirect('business/view2/'.$businessId);
        } else {
          $this->view->disable();
          print_r($claimRequest->getMessages());
        }
    return $this->response->redirect('business/view2/'.$biz_id);
    
    } else {
    $claimRequest = new ClaimRequests();
        $claimRequest->created = date('Y-m-d H:i:s');
        $claimRequest->modified = date('Y-m-d H:i:s');
        $claimRequest->member_id = $userId;
        $claimRequest->business_id = $businessId;
        $claimRequest->status = 'pending'; // default status
    $businessCredential = Business::findFirst("id='".$businessId."'");
    $businessAddress = $businessCredential->street .", ". $businessCredential->city.", ".$businessCredential->state.", ".$businessCredential->country_code_id;
    $userCredential = Members::findFirst("id=".$userId);
        if ($claimRequest->create()) {
    $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Business Claim has been sent.');
    $email = 'jopper_jan@yahoo.co.jp';
    $getBusinessName = Business::findFirst('id='.$businessId);
    $businessName = $getBusinessName->name;
    $this->getDI()->getMail()->send(
                        array($email => ''),
                        'Check on Business Establishment Claim',
                        'claim_req',
                        array( 'first_name' => $userCredential->first_name, 'last_name' => $userCredential->last_name, 'email'=> $userCredential->email, 'address' => $businessAddress, 'business_name' => $businessName, "telephone" =>$alternateNum)
                );
      //return $this->response->redirect('business/view/'. $business_id);
      $this->session->set("claimingsuccess", 1);
          return $this->response->redirect('business/view2/'.$businessId);
        } else {
          $this->view->disable();
          print_r($claimRequest->getMessages());
        }
    //$this->session->set("claimingsuccess", 1);
    return $this->response->redirect('business/view2/'.$businessId);
    }
  }
  }
  
    public function update_businessAction($businessId = null)
    {
        $business = Business::findFirst($businessId);
        if(!$business) {
            return $this->response->redirect('biz/business_search');
        }
        if ($this->request->isPost()) {
            $countryId = $this->request->getPost('country_id');
            $country = Countries::findFirst(array('columns'    => '*', 
                                             'conditions' => 'id LIKE :id:', 
                                             'bind' => array('id' => $countryId)));
            $countryName = '';
            if($country) {
                $countryName = $country->country;
            } 

            $address = str_replace(' ', '+', $this->request->getPost('street').'+'.$this->request->getPost('city').'+'.$countryName);
            $userSession = $this->session->get("userSession");
            $content = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyAbpLPfBH8sNdVSzMULD_BZN9qrAqbL3V8');
            $json = json_decode($content, true);
            $lat = $json['results'][0]['geometry']['location']['lat'];
            $lng = $json['results'][0]['geometry']['location']['lng'];
            $business->modified = date('Y-m-d H:i:s'); 
            $business->member_id =  $userSession['id'];
            $business->name = $this->request->getPost('name');
            $business->website = $this->request->getPost('website');
            $business->telephone = $this->request->getPost('telephone');
            $business->street = $this->request->getPost('street');
            $business->city = $this->request->getPost('city');
            $business->country_id = $this->request->getPost('country_id');
            $business->lat = $lat;
            $business->lng = $lng;
            $opened = '';
            if(!empty($this->request->getPost('opened'))) { $opened = 'Opened'; };
            if(empty($this->request->getPost('opened'))) { $opened = 'Opening Soon'; };
            $business->opened = $opened;
            if($business->update()){
                if(!empty($this->request->getPost('business_category_ids'))) {
                    $bCtegories = $this->request->getPost('business_category_ids');
                    $bCtegoryIds = explode(',', $bCtegories);
                    BusinessCategoryLists::find('business_id="'.$businessId.'"')->delete();
                    foreach ($bCtegoryIds as $key => $bCtegoryId) {
                        $businessCategoryLists = new BusinessCategoryLists();
                        $businessCategoryLists->created = date('Y-m-d H:i:s');
                        $businessCategoryLists->business_id = $businessId;
                        $businessCategoryLists->business_category_id = $bCtegoryId;
                        $businessCategoryLists->create();
                    }
                }

                $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Business has been updated');
               
                return $this->response->redirect('business/view2/'.$businessId);
            }
        }
        $this->view->setVar('business', $business);
        $countries = Countries::find();
        $this->view->setVar('countries', $countries);
        $businessCategoryLists = BusinessCategoryLists::find('business_id="'.$businessId.'"');
        $this->view->setVar('businessCategoryLists', $businessCategoryLists);
    }

    public function add_photoAction($id = null) {
  $this->view->setTemplateAfter('default1');
        $userSession = $this->session->get('userSession');
        $id = $userSession['id'];
        $member = Members::findFirst($id);
        
        if(!$member) {
            $this->response->redirect('biz/page/'.$userSession['id']);
        }

        $photos = MemberPhotos::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));

        $this->view->setVars([
            'photos' => $photos, 
            'member' => $member
        ]);

        // POST 
        if($this->request->isPost() && $this->request->hasFiles() == true){
            //ini_set('upload_max_filesize', '64M');
            set_time_limit(1200);
            $uploads = $this->request->getUploadedFiles();

            $isUploaded = false;
            #do a loop to handle each file individually
            foreach($uploads as $upload){
                #define a “unique” name and a path to where our file must go
                $fileName = $upload->getname();
                $fileInfo = new SplFileInfo($fileName);
                $fileExt = $fileInfo->getExtension();
                $fileExt = strtolower($fileExt);
                $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.'.$fileExt;
                //$fileExt = $upload->getExtension();
                $fileImageExt = array('jpeg', 'jpg', 'png');
                //error_log("File Extension :".$fileExt, 0);
                $fileType = '';
                $filePath = '';
                $path = '';
                //$path = ''.$newFileName;
                if(in_array($fileExt, $fileImageExt)){
                    $path = 'img/member/'.$newFileName; // img/business_member ?
                    $filePath = 'img/member/'; // img/business_member ?
                    //$fileType = 'Image';
                }
                #move the file and simultaneously check if everything was ok
                ($upload->moveTo($path)) ? $isUploaded = true : $isUploaded = false;
            }

            #if any file couldn't be moved, then throw an message
            if($isUploaded) {
                $memberPhotos = new MemberPhotos();
                $memberPhotos->created = date('Y-m-d H:i:s'); 
                $memberPhotos->modified = date('Y-m-d H:i:s'); 
                $memberPhotos->member_id =  $userSession['id'];
                $memberPhotos->type = '1';
                $memberPhotos->file_path =  $filePath;
                $memberPhotos->filename =  $newFileName;
                $memberPhotos->caption = $this->request->getPost('caption');
                if(count($photos) > 0) {
                    $memberPhotos->primary_pic = 'No';
                } else {
                    $memberPhotos->primary_pic = 'Yes';
                    $userSession['primary_pic'] = $filePath.$newFileName;
                    $this->session->set('userSession', $userSession);
                }
                
                if($memberPhotos->save()){
                    return $this->response->redirect('biz/add_photo/'.$id);
                } else {
                    // if member photos creation is failed. 
                    $this->view->disable();
                    print_r($memberPhotos->getMessages());
                }
            }
        }

    }

    public function delete_photoAction($id = null) {
        
    }
    

public function biz_claimsearchAction() {
  //$this->view->setTemplateAfter('default1');
    $this->view->setTemplateAfter('oneheader_default');
    if ($this->request->isPost()) {
      $name = $this->request->getPost('name');
      $location = $this->request->getPost('location');
      
      if ($name == '' && $location == '' ) {
        return $this->response->redirect('biz/biz_claimsearch/');
      } else if ($name != '' && $location == '') {
    return $this->response->redirect('biz/biz_claimresult/'.$name .'/xxx');
      } else if ($name == '' && $location != '') {
    return $this->response->redirect('biz/biz_claimresult/xxx/'. $location);
      }
    }
    }

    public function biz_claimresultAction($name = null, $location = null) {
  $this->view->setTemplateAfter('default1');
        $searchWords = '';
        $business = array();

        if (isset($_GET["page"])) {
          $currentPage = (int) $_GET["page"];
        } else {
          $currentPage = 1;
        }

        $conditions = '';
        if ($name == null && $location == null) {
          return $this->response->redirect('biz/biz_claimsearch/');
        } else {
          if ($name != 'xxx') {
            $conditions .= ' OR name LIKE :name:';
            $bind['name'] = '%'.$name.'%';
            $searchWords .= ', '.$name;
          }

          if ($location != 'xxx') {
             $conditions .= ' OR street LIKE :street: OR city LIKE :city:';
                $bind['street'] = '%'.$location.'%';
                $bind['city'] = '%'.$location.'%';
                $searchWords .= ', '.$location;
          }

          $searchWords = substr($searchWords, 2);
          $business = Business::find(array(
            'columns' => '*',
            'conditions' => substr($conditions, 3),
            'bind' => $bind
          ));

          //$this->view->setVar('business', $business);
        }

         $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $business,
                "limit"=> 4,
                "page" => $currentPage
            )
          );
        $page = $paginator->getPaginate();
  $this->view->setVar('business', $page);
    }

    public function set_primary_photoAction($id = null) {
        $this->view->disable();
        $photos = MemberPhotos::findFirst($id);
        $userSession = $this->session->get('userSession');
        if(!$photos || $userSession['id'] != $photos->member_id) {
            return $this->response->redirect('biz/add_photo/'.$userSession['id']);
        } else { 
            $currentPhotos = MemberPhotos::find('member_id = "'.$userSession['id'].'"');
            foreach ($currentPhotos as $key => $currentPhoto) {
                $currentPhoto->primary_pic = 'No';
                if (!$currentPhoto->update()) {
                    $this->view->disable();
                    echo "failed to update current photo.";
                }
               
            }

            $photos->modified = date('Y-m-d H:i:s');
            $photos->primary_pic = 'Yes';
            if($photos->update()) {
              $userSession['primary_pic'] = $photos->file_path.$photos->filename;
        $this->session->set('userSession', $userSession);
                $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Photo has been set as primary.');
                return $this->response->redirect('biz/add_photo/'.$userSession['id']);
            } else {
                $this->view->disable();
                echo "failed to modified data. ";
            }
        }
    }

    public function update_photo_captionAction($id = null) {

    }

    public function manage_adAction($id = null) {
      $this->view->setTemplateAfter('default1');
      $member  = Members::findFirst("id=".$id);
      $check_subscription = MemberSubscriptions::count("member_id='".$id."'");
      if ($check_subscription > 0) {
        $subscription = 1;
      } else {
        $subscription = 0;
      }
      $this->view->setVar('subscription', $subscription);
      $this->view->setVar('member', $member);
        $claimRequests = ClaimRequests::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
        $this->view->setVar('claimRequests', $claimRequests);     
      $imageAds = ImageAds::find("member_id=".$id);
      $this->view->setVar('imageAds', $imageAds);
  }

    public function add_adAction($id = null, $business_id = null) {
  if ($id == null || $business_id = null) {
    return $this->response->redirect('biz/manage_ad');
  } 
    } 
    public function homepage_banner2Action($id = null) {
  if ($id == null) {
    return $this->response->redirect('biz/login');
  }
  
  $member  = Members::findFirst("id=".$id);
  $this->view->setVar('member', $member);
    }
    public function homepage_bannerAction($id = null) {
  if ($id == null) {
    return $this->response->redirect('biz/login');
  }
  
  $member  = Members::findFirst("id=".$id);
  $this->view->setVar('member', $member);

    }

    public function subpage_bannerAction($id = null) {
  if ($id == null) {
    return $this->response->redirect('biz/login');
  }

  $member = Members::findFirst("id=".$id);
  $this->view->setVar('member', $member);
    }

    public function crop_subpagebannerAction() {
  if ($this->request->isPost()) {
            
            $this->view->disable();
            if ($this->request->hasFiles() == true) 
            {
                $this->view->disable();
                echo "has file disable()";
            }
            $encoded = $this->request->getPost('image-data');
            $decoded = urldecode($encoded);
            $exp = explode(',', $decoded);
            $base64 = array_pop($exp);
            $data = base64_decode($base64);
            $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.png';
            $filePath = 'img/';

            $file = $filePath . $newFileName;
            //echo $encoded;
            //echo '<img src="'.$encoded.'">';
            //file_put_contents($file, $data);
            if (file_put_contents($file, file_get_contents($encoded))) {
                    $userSession = $this->session->get("userSession");
                    $businessImages = new ImageAds();
                    $businessImages->created = date('Y-m-d H:i:s'); 
                    $businessImages->modified = date('Y-m-d H:i:s'); 
                    $businessImages->member_id =  $userSession['id'];
                    $businessImages->business_id =  $this->request->getPost('business');
                    $businessImages->file_path =  $filePath;
                    $businessImages->file_name =  $newFileName;
                    $businessImages->type = $this->request->getPost('type');
        $businessImages->content = $this->request->getPost('content');
                    if($businessImages->create()){
                        return $this->response->redirect('business/view2/'.$this->request->getPost('business'));
                    } else {
                        $this->view->disable();
                        print_r($businessImages->getMessages());
                    }
            } else {
    $this->view->disable();
    echo $file;
      }

        }
    
  return $this->response->redirect('business/view2/'. $this->request->getPost('business'));
    }
public function crop_homepagebanner2Action() {
if ($this->request->isPost()) {
            
            $this->view->disable();
            if ($this->request->hasFiles() == true) 
            {
                $this->view->disable();
                echo "has file disable()";
            }
            $encoded = $this->request->getPost('image-data');
            $decoded = urldecode($encoded);
            $exp = explode(',', $decoded);
            $base64 = array_pop($exp);
            $data = base64_decode($base64);
            $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.png';
            $filePath = 'img/ad/';

            $file = $filePath . $newFileName;
            //echo $encoded;
            //echo '<img src="'.$encoded.'">';
            //file_put_contents($file, $data);
            if (file_put_contents($file, file_get_contents($encoded))) {
                    $userSession = $this->session->get("userSession");
                    $businessImages = new ImageAds();
                    $businessImages->created = date('Y-m-d H:i:s'); 
                    $businessImages->modified = date('Y-m-d H:i:s'); 
                    $businessImages->member_id =  $userSession['id'];
                    $businessImages->business_id =  $this->request->getPost('business');
                    $businessImages->file_path =  $filePath;
                    $businessImages->file_name =  $newFileName;
                    $businessImages->type = $this->request->getPost('type');
                    $businessImages->side_text = $this->request->getPost('side_text');
                    $businessImages->bground_color = $this->request->getPost('bcolor');
                    $businessImages->status = '1';

                    if($businessImages->create()){
                        return $this->response->redirect('business/view2/'.$this->request->getPost('business'));
                    } else {
                        $this->view->disable();
                        print_r($businessImages->getMessages());
                    }
            } else {
    $this->view->disable();
    echo $file;
      }

        }
    
  return $this->response->redirect('business/view2/'. $this->request->getPost('business'));
    }
public function crop_homepagebannerAction() {
if ($this->request->isPost()) {
            
            $this->view->disable();
            if ($this->request->hasFiles() == true) 
            {
                $this->view->disable();
                echo "has file disable()";
            }
            $encoded = $this->request->getPost('image-data');
            $decoded = urldecode($encoded);
            $exp = explode(',', $decoded);
            $base64 = array_pop($exp);
            $data = base64_decode($base64);
            $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.png';
            $filePath = 'img/ad/';

            $file = $filePath . $newFileName;
            //echo $encoded;
            //echo '<img src="'.$encoded.'">';
            //file_put_contents($file, $data);
            if (file_put_contents($file, file_get_contents($encoded))) {
                    $userSession = $this->session->get("userSession");
                    $businessImages = new ImageAds();
                    $businessImages->created = date('Y-m-d H:i:s'); 
                    $businessImages->modified = date('Y-m-d H:i:s'); 
                    $businessImages->member_id =  $userSession['id'];
                    $businessImages->business_id =  $this->request->getPost('business');
                    $businessImages->file_path =  $filePath;
                    $businessImages->file_name =  $newFileName;
                    $businessImages->type = $this->request->getPost('type');
                    $businessImages->status = '1';

                    if($businessImages->create()){
                        return $this->response->redirect('business/view2/'.$this->request->getPost('business'));
                    } else {
                        $this->view->disable();
                        print_r($businessImages->getMessages());
                    }
            } else {
    $this->view->disable();
    echo $file;
      }

        }
    
  return $this->response->redirect('business/view2/'. $this->request->getPost('business'));
    }

    // MOBILE BANNER AD
    public function mobile_bannerAction($id = null) {
      if ($id == null) {
      return $this->response->redirect('biz/login');
    }
    
    $member  = Members::findFirst("id=".$id);
    $this->view->setVar('member', $member);
    }

    // WEBSITE BANNER AD 
    public function website_bannerAction($id = null) {
      if ($id == null) {
      return $this->response->redirect('biz/login');
    }
    
    $member  = Members::findFirst("id=".$id);
    $this->view->setVar('member', $member);
    }

    // GET IMAGE PREVIEW AND PUT ON MODAL
    public function getImagePreviewAction() {
      if($this->request->isAjax()) 
        {
          $imageId = $this->request->getQuery('image_id');
            $arrFlag = array();

            $image = ImageAds::findFirst("id='".$imageId."'");

            $file = $this->url->getBaseUri().$image->file_path.$image->file_name;
            $taggedFile = '<img src="'.$file.'"/>';
            $arrFlag[] = ["file" => $taggedFile];

            $payload     = $arrFlag; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;

        }
      

    }

    public function adcampaignAction($id = null, $business_id = null) {
      $member  = Members::findFirst("id='".$id."'");
      $this->view->setVar('member', $member);
      $claimRequests = ClaimRequests::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('claimRequests', $claimRequests); 

      if ($this->request->isPost()) {
        $businessId = $this->request->getPost('business');
        $business = Business::findFirst("id='".$businessId."'");
        $this->view->setVars([
          'business' => $business,
          'postContent'  => $this->request->getPost('content')
        ]);
      }
    }

    public function setcampaignadAction() {
        if($this->request->isAjax()) 
        {
            $memberId = $this->request->getQuery('member_id');
            $businessId = $this->request->getQuery('business_id');
            $content = $this->request->getQuery('content');

            $arrFlag = array();

            $check_biz_ad = Adcampaigns::count("business_id='".$businessId."'");
            if ($check_biz_ad > 0) {
              $updateCampaign = Adcampaigns::findFirst("business_id='".$businessId."'");
              $updateCampaign->modified = date('Y-m-d H:i:s');
              $updateCampaign->member_id = $memberId;
              $updateCampaign->content = $content;

              if ($updateCampaign->update()) {
                $result = "Updating Ad Campaign Success";
              } else {
                $result = "Updating Ad Campaign Failed";
              }
            } else {
              $newAdCampaign = new Adcampaigns();
              $newAdCampaign->created = date('Y-m-d H:i:s'); 
              $newAdCampaign->modified = date('Y-m-d H:i:s'); 
              $newAdCampaign->business_id = $businessId;
              $newAdCampaign->member_id = $memberId;
              $newAdCampaign->content = $content;

              if ($newAdCampaign->create()) {
                $result = 'Adding new Ad Campaign Success';
              } else {
                $result = 'Adding new Ad Campaign Failed';
              }
            }
            $arrFlag[] = ["count_result" => $countResult, "status_result" => $result];

            $payload     = $arrFlag; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;

        }
    }

}
