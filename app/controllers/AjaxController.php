<?php


class AjaxController extends ControllerBase {

    public function initialize(){
        
        parent::initialize();
    }


  public function getCitiesAction() {

        // This is an Ajax response so it doesn't generate any kind of view
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($this->request->isAjax()) {
            $arrCities = array();
             $stateCode = $this->request->getQuery('param1');
            $cities = Cities::find("state_code_id='" . $stateCode . "'");

            foreach ($cities as $city) {
                $arrCities[] = ['id' => $city->id, 'name' => $city->city_name];
            }
            $payload     = $arrCities; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }





    public function getStatesAction() {

        // This is an Ajax response so it doesn't generate any kind of view
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);


        if ($this->request->isAjax()) {
            $arrStates = array();
            $countryCode = $this->request->getQuery('param');


            $states = States::find("country_code_id='" . $countryCode . "'");

            foreach ($states as $state) {
                $arrStates[] = ['id' => $state->id, 'name' => $state->state_name];
            }

            $payload     = $arrStates; //array(1, 2, 3);
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

          

            return $response;
        }
    }


    public function ajax_classifiedsAction(){

        // This is an Ajax response so it doesn't generate any kind of view
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

        //if ajax request run
        if ($this->request->isAjax()) {

            //get the parameters
            $keyword = $this->request->getQuery('keyword');
            $parent = $this->request->getQuery('parent');



            // variables default
            $conditions = ''; 
            $bind = array();


            if(!empty($keyword)) {
                $conditions .= ' name LIKE :name: OR brand LIKE :brand: OR model LIKE :model: ';
                $bind['name'] = '%'.$keyword.'%';
                $bind['brand'] = '%'.$keyword.'%';
                $bind['model'] = '%'.$keyword.'%';
            }
            // $conditions .= ($keyword != '' AND $city_code != '')? 'AND' : '' ;
            // if(!empty($city_code)) {
            //     $conditions .= ' city LIKE :city: ';
            //     $bind['city'] = '%'.$city_code.'%';
            // }


            //queryload
            $autos = Automotives::find([
                'columns' => '*',
                'conditions' => $conditions,
                'bind' => $bind,
                'order' => 'id asc',
                'limit' => '10'
            ]);


            //fetch the addres list
            foreach ($autos as $auto) {

                // put in bold the written text
               //  $country_name = preg_replace("/".$keyword."/i", "<b>\$0</b>", $auto->name.' '.$auto->brand.' '.$auto->model);

                // get the country name to another table CountryCodes
                 //$country = CountryCodes::findFirst("id = '$rs->country_code_id' ");
                
                //display the list: onclick go to designated id input field
                // echo '<li id ="'.$keyword.'" onclick="set_item(\''.str_replace("'", "\'", $rs->city_name.', '.$rs->state_code_id).', '.$country->country_name.'\',\''.$parent.'\');set_code(\''.$rs->id.'\',\''.$parent.'\')"><i class="fa fa-map-marker"></i> '.$country_name.', '.$rs->state_code_id.' '.$country->country_name.'</li>';
                echo '<li >';
                echo $auto->name.'<br>';
                echo $auto->brand.'<br>';
                echo $auto->model;
                echo '</li>';




            }//end of for each


        }

    }



}

?>