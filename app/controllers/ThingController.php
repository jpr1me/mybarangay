<?php
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;


class ThingController extends ControllerBase {

    public function initialize()
    {
        parent::initialize();

        
    }

  public function getCitiesAction() {
        if ($this->request->isAjax()) {
            $arrCities = array();
             $stateCode = $this->request->getQuery('param1');
            $cities = Cities::find("state_code_id='" . $stateCode . "'");

            foreach ($cities as $city) {
                $arrCities[] = ['id' => $city->id, 'name' => $city->city_name];
            }
            $payload     = $arrCities; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }


    public function getStatesAction() {

        if ($this->request->isAjax()) {
            $arrStates = array();
            $countryCode = $this->request->getQuery('param');


            $states = States::find("country_code_id='" . $countryCode . "'");

            foreach ($states as $state) {
                $arrStates[] = ['id' => $state->id, 'name' => $state->state_name];
            }

            $payload     = $arrStates; //array(1, 2, 3);
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            return $response;
        }
    }

   /**
     * [flagAction description]
     * @return [type] [description]
     */
    public function flagAction(){

        $this->view->disable();



        if($this->request->isAjax()) 
        {
      
            $review = $this->request->getQuery('review');
            $reviewer = $this->request->getQuery('reviewer');
            $flagger = $this->request->getQuery('flagger');
            $flag_type = $this->request->getQuery('flag_type');
            $referer = $this->request->getHTTPReferer (); 
            $controller = 'things';

            $newFlag = new Flags();
            $newFlag->created = date('Y-m-d H:i:s');
            $newFlag->modified = date('Y-m-d H:i:s');
            $newFlag->review_id = $review;
            $newFlag->member_id = $flagger;
            $newFlag->target_id = $reviewer;
            $newFlag->flag_type_id = $flag_type;
            $newFlag->location = $controller; 
            $newFlag->status = '0';
            $newFlag->page = $referer;

            if ($newFlag->create()) {
                $resultInfo = 'success';
            } else {
                $resultInfo = 'failed';
            }
            
            $arrayName = array('review' => $review, 'reviewer' => $reviewer, 'flagger' => $flagger, 'result' => $resultInfo);
            echo json_encode(array('result' => 'OK', 'items' => $arrayName));
        }


    }






	public function formValidate() {
		$validation = new Phalcon\Validation();
		$validation->add('name', new PresenceOf(array(
			'message' => 'name field is required'	
		)));
		$validation->add('details', new PresenceOf(array(
			'message' => 'details field is required'	
		)));
		$validation->add('price', new PresenceOf(array(
			'message' => 'price field is required'	
		)));
		$validation->add('thing_condition_id', new PresenceOf(array(
			'message' => 'condition field is required'	
		)));
		$validation->add('thing_category_id', new PresenceOf(array(
			'message' => 'category field is required'	
		)));

		return $validation->validate($_POST);
	
	}

   /**
    * [indexAction description]
    * @return [type] [description]
    */
   public function indexAction(){   

     $this->view->setTemplateAfter('default1');
     $this->view->setVar('breadcmp', 'Things');

        $things = '';
        $city = '';
        $city_code = '';
        $name = '';
        $thing_category_id = '';
        $thing_condition_id = '';
        $minPrice = '';
        $maxPrice = '';
        $btn_search = '';
        $sql = "SELECT * FROM Things ";
        if (isset($_GET['btn_search'])) {

            $locations = [];
            $getters = [];
            $queries = [];
            foreach ($_GET as $key => $value) {
               $temp = is_array($value) ? $value : trim($value) ;
               if(!empty($temp)){
                    list($key) = explode("-", $key);
                    if($key == 'name'){
                        array_push($locations, $value);
                    }
                    if(!in_array($key, $getters)){
                        $getters[$key] = $value;
                    }
               }
            }
            if(!empty($locations)){
                $loc_qry = implode(",", $locations);
            }
            if(!empty($getters)){
                foreach ($getters as $key => $value) {
                   ${$key} = $value;
                    switch ($key) {
                        case 'name':
                               array_push($queries, "(name LIKE '%$name%')");
                            break;
                        case 'thing_category_id':
                               array_push($queries, "(thing_category_id = $thing_category_id)");
                            break;
                        case 'thing_condition_id':
                               array_push($queries, "(thing_condition_id = $thing_condition_id)");
                            break;
                        case 'minPrice':
                        break;
                        case 'maxPrice':
                        break;
                        case 'btn_search':
                            if(!empty($minPrice) AND !empty($maxPrice)){
                                array_push($queries, "(price BETWEEN $minPrice AND $maxPrice )");
                            }else{
                                if(!empty($minPrice)){
                                    array_push($queries, "(price < $minPrice )");
                                }
                                if(!empty($maxPrice)){
                                    array_push($queries, "(price > $maxPrice )");
                                }
                            }
                        break;   
                        case 'city_code':
                            array_push($queries, "(city = $city_code)");
                        break;
                        case 'city':
                        break;

                        default:
                            # code...
                            break;
                    }
                }
            }
            if(!empty($queries)){
                $sql .= " WHERE ";
                $i = 1;
                foreach ($queries as $query) {
                    if($i < count($queries)){
                        $sql .= $query." AND ";
                    }else{
                        $sql .= $query;
                    }
                    $i++;
                }

            }

     // $this->view->disable();
        }

        
        $things = $this->modelsManager->executeQuery($sql);
        $this->view->setVars([
        'things' => $things,
        'name' => $name, 
        'thing_category_id' => $thing_category_id, 
        'thing_condition_id' => $thing_condition_id, 
        'btn_search' => $btn_search,
        'minPrice' => $minPrice,
        'maxPrice' => $maxPrice,
        'city' => $city,
        'city_code' => $city_code
        ]);

     $this->view->setVar('thing_categories', ThingCategories::find());

    }

    public function update_photoAction($id = null){

        $this->view->disable();
        if ($this->request->isPost()) {


                if($this->request->hasFiles() == true) {


                    $userSession = $this->session->get("userSession");
                    // $this->folderOrganizer('classfieds', $id);
                    $uploads = $this->request->getUploadedFiles();

                    // upload default to false
                    $isUploaded = false;

                    //allowed file extension
                    $fileImageExt = array('jpeg', 'jpg', 'png');

                    $filePath = 'img/thing/';

                    $fileType = '';

                    foreach ($uploads as $upload) {

                        $fileName = $upload->getname() ;

                        $fileInfo = new SplFileInfo($fileName);

                        $fileExt  = strtolower($fileInfo->getExtension());

                        // filename convert to md5
                        $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'-th.'.$fileExt;

                        $path = NULL;

                        //
                        if(in_array($fileExt, $fileImageExt)){
                                $path = 'img/thing/'.$newFileName;
                        }

                       //move the file and simultaneously check if everything was ok
                       ($path != NULL OR !empty($path)) ? $isUploaded = true : $isUploaded = false;

                        if ($isUploaded) {
                            $photo = new ThingPhotos();
                            $photo->created = date('Y-m-d H:i:s');
                            $photo->modified = date('Y-m-d H:i:s');
                            $photo->member_id = $userSession['id'];
                            $photo->thing_id = $id;
                            $photo->file_path = $filePath;
                            $photo->filename = $newFileName;
                           
                            if ($photo->create()) {
                                $upload->moveTo($path);
                                $this->session->set('successMessasge', 'Photo Succesfuly Added');
                            } else {
                                $error = '';
                                //print_r($photo->getMessages());
                                foreach ($photo->getMessages() as $error) {
                                   $error .= $photo->getMessage.'</br>';
                                }
                                 $this->session->set('errorMessasge', 'Uploading Error </br>' .$error);
                            }
                        }

                    }

                }

            $this->response->redirect('thing/update/'.$id);
        }else{

            $this->response->redirect('thing/update/'.$id);
        }

    }


    public function delete_photoAction($id = null, $itemid = null){


        $photo = ThingPhotos::findFirst($id);
       
        $trashImg = 'img/thing/'.$photo->filename;
        unlink($trashImg);
        
        if ($photo->delete()) {
            $this->session->set('successMessasge', 'Photo Succesfuly Deleted');

        }else{
            $this->session->set('successMessasge', 'Error can\'t delete');
        }
        $this->response->redirect('thing/update/'.$itemid);

    }


    public function viewxAction($id = null)
    {
      $thing = Things::findFirst($id);

      if (!$thing) {
        return $this->response->redirect('thing/index');
      }

      $this->view->setVar('thing', $thing);
    }


    public function viewAction($id = null){

    $this->view->setTemplateAfter('default1');
      $thing = Things::findFirst($id);

      if (!$thing) {
        return $this->response->redirect('thing/index');
      }

      $this->view->setVar('thing', $thing);

     $this->view->setVar('thing_categories', ThingCategories::find());
      

    }


    public function view2Action($id = null){


	$this->view->setTemplateAfter('default1');
      $thing = Things::findFirst($id);

      if (!$thing) {
        return $this->response->redirect('thing/index');
      }

      $this->view->setVar('thing', $thing);

     $this->view->setVar('thing_categories', ThingCategories::find());
      
    }

    public function updateAction($id = null, $valuex = null){   

     $this->view->setTemplateAfter('default1');

        if ($this->request->isPost()) {
        
        $messages = $this->formValidate();
        if (count($messages)) {
     
            $errorMsg = '';
            foreach ($messages as $msg) {
              $errorMsg .= $msg . "<br>";       
            }
            $this->session->set('errorMessasge', $errorMsg);
            return $this->response->redirect('thing/update/'.$id);   
        }


            $userSession = $this->session->get("userSession");
            $thing = Things::findFirst($id);
            $thing->modified = date('Y-m-d H:i:s');
            $thing->member_id = $userSession['id'];
            $thing->name = $this->request->getPost('name');
            $thing->price = str_replace(',', '', $this->request->getPost('price'));
            $thing->thing_category_id = $this->request->getPost('thing_category_id');
            $thing->thing_condition_id = $this->request->getPost('thing_condition_id');
            $thing->details = $this->request->getPost('details');
            $thing->contact_no = $this->request->getPost("contact_no");
            $thing->email = $this->request->getPost("email");
            $thing->other_name =$this->request->getPost("other_name");
            $thing->currency_type = $this->request->getPost("currency_type");
            $thing->street = $this->request->getPost("street");
            $thing->country_code_id = $this->request->getPost("country_id");
            $thing->state = $this->request->getPost("state");
            $thing->city =$this->request->getPost("city");
            

                if($thing->update()){
         
                   $this->session->set('successMessasge', 'Information Successfully Update');
                }else{
                    foreach ($thing->getMessages() as $error) {
                       $errorMsg .= $error->getMessage() .'</br>';
                    }
                    $this->session->set('errorMessasge', $errorMsg);
                }  



          }



          $thingPhotos = ThingPhotos::find('thing_id =' . $id);
          $this->view->setVar('thingPhotos', $thingPhotos);


          $thing = Things::findFirst($id);
          if (!$thing) {
            return $this->response->redirect('thing/index');
          }

          $this->view->setVar('thing', $thing);

          $thingConditions = ThingConditions::find();
          $this->view->setVar('thingConditions', $thingConditions);
          $thingCategories = ThingCategories::find();
          $this->view->setVar('thingCategories', $thingCategories);
    }

    public function add_photoAction($id = null)
    {
      
    }

    public function update_photo_captionAction($id = null, $photoId = null)
    {
      
    }
   

}

