<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{

	public function initialize()
	{
		$this->view->setTemplateAfter('default1');
		$this->session;
		$this->fbSetDefault;
		$this->cookie_login();
		$userSession = array();
		if ($this->session->has("userSession")) {
           	//Retrieve its value
        	$userSession = $this->session->get("userSession");
           	//$this->view->setVar("firstName", $userSession['first_name']);
        }
        //error_log($userSession['primary_pic']); 
        $this->view->setVar('userSession', $userSession);
	}

	public function beforeExecuteRoute()
    {
        $userSession['type'] = '';
		if ($this->session->has("userSession")) {
           	//Retrieve its value
        	$userSession = $this->session->get("userSession");
        } 
        $permission = $this->permission($userSession['type']);
        if($permission == false) {
        	//$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Please login.');
			return $this->response->redirect('login');
		}
    }

	public function permission($type = null) {
		$controller = $this->dispatcher->getControllerName();
		$action = $this->dispatcher->getActionName();
		$permission = false;

		// test controllers 
		if($controller == 'payment' && in_array($action, array('testing')))
		{
			$permission = true;
		}
		if($controller == 'yeppie' && in_array($action, array('testing')))
		{
			$permission = true;
		}
		// end test controllers

		if($controller == 'vlog' && in_array($action, array('index')))
		{
			$permission = true;
		}

		if($controller == 'admincontrol' && in_array($action, array('show404','control', 'index','claimbizreq','getuserdata','user_profile', 'reviewflag','getbiz_flag_data','classifiedflag','jobflag', 'thingflag'))){
			$permission = true;
		}

		if($controller == 'index' && in_array($action, array('ajaxAddress', 'ajaxAddress2', 'show404','knowl_center','index', 'info', 'admin_index', 'search', 'search1','admin_approve_biz_claim', 'admin_cancel_request','admin_reject_request', 'index2','admin_flags'))){
			$permission = true;
		}
		if($controller == 'member' && in_array($action, array('signup_success','page', 'photos', 'biz_page', 'advertiser_signup', 'advertiser_login', 'advertiser_emailConfimation','signup', 'gallery', 'resetPassword', 'forgotPassword', 'login'))){
			$permission = true;
		}
		if($controller == 'job' && in_array($action, array('index', 'view2'))){
			$permission = true;
		}
		if($controller == 'real_estate' && in_array($action, array('index', 'view2'))){
			$permission = true;
		}
		if($controller == 'thing' && in_array($action, array('index', 'view2'))){
			$permission = true;
		}
		if($controller == 'review' && in_array($action, array('search_business','search_business2', 'index2', 'search_business1'))){
				$permission = true;
		}
		if($controller == 'business' && in_array($action, array('index', 'view', 'photos', 'admin_claim_business', 'add_photo_cover','admin_approve_biz_claim','reject_biz_claim','cancel_biz_claim', 'filtered_seemore', 'view2', 'gallery', 'getdirections', 'test'))){
				$permission = true;
		}
		if($controller == 'car_and_truck' && in_array($action, array('index',  'view2'))){
			$permission = true;
		}

		if($controller == 'events' && in_array($action, array('search_events', 'view', 'new'))){
			$permission = true;
		}
		if($controller == 'event' && in_array($action, array( 'load_ajax', 'index', 'view', 'new', 'search'))){
			$permission = true;
		}

		if($controller == 'user' && in_array($action, array('admin_login', 'admin_logout', 'admin_add'))){
			$permission = true;
		}

		if($controller == 'biz' && in_array($action, array('signup', 'login', 'logout', 'emailConfimation', 'business_search', 'claim', 'page', 'respond', 'add_photo', 'biz_claimsearch', 'biz_claimresult','claimreq_success'))){
			$permission = true;
		}


		if($controller == 'forum' && in_array($action, array('latest','popular','viewTopic','list_topic','persistent','load_ajax','test','index','view', 'topic_list', 'search'))){
			$permission = true;
		}

		if($controller == 'ajax' && in_array($action, array('index', 'getCities', 'getStates', 'ajax_classifieds'))){
			$permission = true;
		}


		if($type == 'User') {
			if($controller == 'business' && in_array($action, array('admin_index', 'admin_view', 'admin_pending', 'approve','admin_approve_biz_claim','reject_biz_claim','cancel_biz_claim'))){
				$permission = true;
			}
			if($controller == 'business_update' && in_array($action, array('admin_view', 'approve', 'deny'))){
				$permission = true;
			}


		}
		
		if($type == 'Business') {
			if($controller == 'payment' && in_array($action, array('testing', 'template', 'pricetable', 'pay','process','proceed')))
			{
				$permission = true;
			}
		 	if($controller == 'biz' && in_array($action, array('update_business','page1', 'add_photo', 'add_photo1', 'claimreq_success', 'manage_ad', 'add_ad', 'homepage_banner', 'homepage_banner2', 'subpage_banner', 'adcampaign', 'mobile_banner', 'website_banner'))){
		 		$permission = true;
		 	}
		 	if($controller == 'business' && in_array($action, array('add_photo', 'update', 'view2', 'add_image', 'add_image_cover', 'gallery', 'photos','getdirections','new_biz_hours'))){
				$permission = true;
			}
			if($controller == 'review' && in_array($action, array('search_business', 'add', 'update', 'new_business', 'update_business','getStates', 'getCities'))){
				$permission = true;
			}

			if($controller == 'member' && in_array($action, array('add_photo'))){
			$permission = true;
			}

			if($controller == 'add' && in_array($action, array('new_ad'))){
			$permission = true;
			}

			if($controller == 'index' && in_array($action, array('index'))){
			$permission = true;
			}

			if($controller == 'payment' && in_array($action, array('template'))){
			$permission = true;
			}
		}


		if($type == 'Advertiser') {
		 	// if($controller == 'member' && in_array($action, array('adv','add_photo'))){
		 	// 	$permission = true;
		 	// }
		 }

		if($type == 'Member') {

			if($controller == 'forum' && in_array($action, array('view_edit','topicList','editTopic','deleteTopic','deleteReplyTopic','editReplyTopic','addTopic','reply','compliment','add'))){
				$permission = true;
			}
			if($controller == 'index' && in_array($action, array('index', 'index1', 'admin_cancel_request','admin_reject_request','index2'))){
				$permission = true;
			}
			if($controller == 'member' && in_array($action, array('update_photo','pageu','profileu','galleryu','updateu','update', 'add_photo', 'set_primary_photo', 'delete_photo', 'update_photo_caption','signup', 'change_password', 'gallery', 'update_image_caption', 'add_image', 'upload_image'))){
				$permission = true;
			}
			if($controller == 'business' && in_array($action, array('add_photo', 'add_photo1', 'update', 'add_image','view2', 'add_video', 'cropit', 'compliment', 'gallery', 'view3', 'flag', 'new_biz_hours', 'picture_editor', 'getdirections'))){
				$permission = true;
			}
			if($controller == 'review' && in_array($action, array('search_business','search_business2', 'search_business1', 'add', 'update', 'new_business', 'update_business', 'getStates', 'getCities'))){
				$permission = true;
			}
			if($controller == 'job' && in_array($action, array('index',  'index1', 'new', 'update', 'view2','flag','update_photo'))){
				$permission = true;
			}
			if($controller == 'real_estate' && in_array($action, array('index', 'new', 'update', 'view2','flag','update_photo','delete_photo'))){
				$permission = true;
			}
			if($controller == 'thing' && in_array($action, array('index', 'new', 'update', 'view2','flag','update_photo','delete_photo'))){
				$permission = true;
			}
			if($controller == 'car_and_truck' && in_array($action, array('delete_list', 'list','index', 'new',  'update', 'flag','update_photo','delete_photo'))){
				$permission = true;
			}
			if($controller == 'biz' && in_array($action, array('claimreq_success'))){
		 		$permission = true;
		 	}

			if($controller == 'event' && in_array($action, array('load_ajax', 'index', 'view', 'new', 'search', 'update', 'list', 'delete_list'))){
				$permission = true;
			}





		}

		return $permission;
	}
	
	public function cookie_login(){
		if(isset($_COOKIE['mid']) && isset($_COOKIE['e']) && isset($_COOKIE['token'])) {
			$id = $this->decrypt($_COOKIE['mid']);
			$email = $this->decrypt($_COOKIE['e']);
			$token = $this->decrypt($_COOKIE['token']);
			$member = Members::findFirst(array('id = "'.trim($id).'"', 'email = "'.trim($email).'"'));
			//$member =  Members::findFirst(array('id= "'.$id.'"', 'email="Yes"'));
			if ($member == true && $this->security->checkHash($token, $member->cookie_token)) {
				$userSession = get_object_vars($member);
				$profilePic = MemberPhotos::findFirst(array('member_id="'.$userSession['id'].'"', 'primary_pic="Yes"'));
				//$userSession['primary_pic'] = $profilePic->file_path.$profilePic->filename;
				return $this->session->set('userSession', $userSession);
			}
		}
	}
	

	function encrypt($pure_string) {
		$encryption_key = 'sMeynBiaprpainlgiyhP';
		$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
		return $encrypted_string;
	}

	function decrypt($encrypted_string) {
		$encryption_key = 'sMeynBiaprpainlgiyhP';
		$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
		return $decrypted_string;
    }



    /**
     * [folderOrganizer description]
     * @param  [type] $id     [description]
     * @param  [type] $folder [description]
     * @return [type]         [description]
     */
    public function folderOrganizer($id, $folder ){

		$path = '/img/userFiles/'.$id.'/'.$folder;

		if(!file_exists($path)){
			mkdir($path, 0775, true);
		}

		return true;
    }


    public function flagAllAction($pagetype = null) {


	$this->view->disable();



        if($this->request->isAjax()) 
        {
            $arrFlag = array();



            if($pagetype == '1auto'){

	            $review = $this->request->getQuery('review');
	            $reviewer = $this->request->getQuery('reviewer');
	            $flagger = $this->request->getQuery('flagger');
	            $flag_type = $this->request->getQuery('flag_type');
	            $referer = $this->request->getHTTPReferer (); 
	            $controller = 'car_and_truck';
            	
            }




            $newFlag = new Flags();
            $newFlag->created = date('Y-m-d H:i:s');
            $newFlag->modified = date('Y-m-d H:i:s');
            $newFlag->review_id = $review;
            $newFlag->member_id = $flagger;
            $newFlag->target_id = $reviewer;
            $newFlag->flag_type_id = $flag_type;
            $newFlag->location = $controller; 
            $newFlag->status = '0';
            $newFlag->page = $referer;

            if ($newFlag->create()) {
                $resultInfo = 'success';
            } else {
                $resultInfo = 'failed';
            }


            $arrFlag[] = ["review" => $review, "reviewer" => $reviewer, "flagger" => $flagger, 'result' => $resultInfo];

            $payload     = $arrFlag; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }




            // $review = $this->request->getQuery('review');
            // $reviewer = $this->request->getQuery('reviewer');
            // $flagger = $this->request->getQuery('flagger');
            // $flag_type = $this->request->getQuery('flag_type');
            // $referer = $this->request->getHTTPReferer (); 
            // $controller = 'business';

            // $newFlag = new Flags();
            // $newFlag->created = date('Y-m-d H:i:s');
            // $newFlag->modified = date('Y-m-d H:i:s');
            // $newFlag->review_id = $review;
            // $newFlag->member_id = $flagger;
            // $newFlag->target_id = $reviewer;
            // $newFlag->flag_type_id = $flag_type;
            // $newFlag->location = $controller; 
            // $newFlag->status = '0';
            // $newFlag->page = $referer;

            // if ($newFlag->create()) {
            //     $resultInfo = 'success';
            // } else {
            //     $resultInfo = 'failed';
            // }

            // $arrFlag[] = ["review" => $review, "reviewer" => $reviewer, "flagger" => $flagger, 'result' => $resultInfo];

            // $payload     = $arrFlag; 
            // $status      = 200;
            // $description = 'OK';
            // $headers     = array();
            // $contentType = 'application/json';
            // $content     = json_encode($payload);

            // $response = new \Phalcon\Http\Response();

            // $response->setStatusCode($status, $description);
            // $response->setContentType($contentType, 'UTF-8');
            // $response->setContent($content);

            // // Set the additional headers
            // foreach ($headers as $key => $value) {
            //    $response->setHeader($key, $value);
            // }

            // $this->view->disable();

            // return $response;

        } else {
            $this->view->disable();
            echo "flag action failed";
        }
    }






}
