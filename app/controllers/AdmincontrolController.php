<?php

class AdmincontrolController extends ControllerBase
{
	public function indexAction() {

		$countUsers = Members::count();
		$countReviews = Reviews::count();
		$countFlags = Flags::count();
		$countBusiness = Business::count();
		$this->view->setVars(['numMember' => $countUsers, 'numReview' => $countReviews, 
			'numFlag' => $countFlags, 'numBiz' => $countBusiness]);

		$this->view->setTemplateAfter('admin-panel');
	}

	public function claimbizreqAction() {
		$this->view->setTemplateAfter('admin-panel');
		$claimRequests = ClaimRequests::find();
        $this->view->setVar('requests', $claimRequests);
	}

	public function getuserdataAction() {
		if ($this->request->isAjax()) {
            $userArr = array();
            $members = Members::find();

            foreach ($members as $member) {
            	$viewLink = $this->url->getBaseUri() . 'admincontrol/user_profile/'.$member->id;
                $userArr[] = [$member->first_name,
                $member->middle_name,$member->last_name,
                $member->gender,$member->birthday,
                $member->street,$member->city,
                $member->country_id,$member->email,
                "<a href='".$viewLink."'><i class='icon-eye'></i></a>"];
            }

            $payload     = $userArr; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode(array("data" => $payload));

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
	}
	/*
	 * AJAX REQUESTS
	 *
	 */
	public function getbiz_flag_dataAction() {
		if ($this->request->isAjax()) {
            $reqArr = array();
			$flagTypeId = $this->request->getQuery("flag_type_id");
            $requests = Flags::find("flag_type_id'".$flagTypeId."' AND location='business'");
			
            foreach ($requests as $req) {
            	$reviewId = $req->review_id;
            	$reporter = $req->members->first_name.' '.$req->members->last_name;
				$target = Members::findFirst("id='".$req->target_id."'");
            	$targetOne = $target->first_name.' '.$target->last_name;
            	$location = $req->page;
				if ($req->status == 0) {
					$getStatus = 'pending';
				} else if ($req->status == 1) {
					$getStatus = 'approved';
				} else if ($req->status == 2) {
					$getStatus = 'rejected';
				}
            	$status = $getStatus;
            	$actionLink = '<a href="#" class="btn btn-square btn-success btn-xs"><i class="icon-checkmark-2"></i>
						                	</a> | <a href="#" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i>
						                	</a>';
				$reportDate = $req->created;
				$getReview = Reviews::findFirst("id='".$flag->review_id."'");
				//$dateOfPost = Reviews::findFirst("id='".$reviewId."'");
				$postDate = $getReview->created;
				$getBusiness = Business::findFirst("id='".$getReview->business_id."'");
				$businessName = $getBusiness->name;
				// reportedby, fullname, business name, location(page), date of report, date of post, status , action
                $reqArr[] = [$reporter, $targetOne, $businessName, $location, $req->created, $postDate, $status, $actionLink];
            }
	
			$payload     = $reqArr; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode(array("data" => $payload));

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);
			
            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }
			//$this->view->disable();
            $this->view->disable();
            return $response;
        }
	}
	public function get_flag_type_detailAction() {
		if ($this->request->isAjax()) {
            $reqArr = array();
			$flagTypeId = $this->request->getQuery("flag_type_id");
            $request = FlagTypes::findFirst('id = "'.$flagTypeId.'"');
			
            $reqArr[] = $request->detail;

            $payload     = $reqArr; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode(array("type_name" => $payload));

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
	}
	// public function getclaimrequestAction() {
	// 	if ($this->request->isAjax()) {
 //            $reqArr = array();
 //            $requests = ClaimRequests::find('status = "pending"');

 //            foreach ($requests as $req) {
 //            	$bizId = $req->business->id;
 //            	$bizName = $req->business->name;
 //            	$fullName = $req->members->first_name.' '.$req->members->last_name;
 //            	$email = $req->members->email;
 //            	$status = $req->status;
 //            	$contact = $req->members->telephone;
 //            	$actionLink = '<a href="#" class="btn btn-square btn-success btn-xs"><i class="icon-checkmark-2"></i>
	// 					                	</a> | <a href="#" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i>
	// 					                	</a>';
 //                $reqArr[] = [$fullName, $bizName, $email, $contact, $req->created,
 //                $status, $actionLink];
 //            }

 //            $payload     = $reqArr; 
 //            $status      = 200;
 //            $description = 'OK';
 //            $headers     = array();
 //            $contentType = 'application/json';
 //            $content     = json_encode(array("data" => $payload));

 //            $response = new \Phalcon\Http\Response();

 //            $response->setStatusCode($status, $description);
 //            $response->setContentType($contentType, 'UTF-8');
 //            $response->setContent($content);

 //            // Set the additional headers
 //            foreach ($headers as $key => $value) {
 //               $response->setHeader($key, $value);
 //            }

 //            $this->view->disable();

 //            return $response;
 //        }
	// }

	public function user_profileAction($id = null) {
		$this->view->setTemplateAfter('admin-panel');
	}
	
	/*
	 * FLAGS
	 */
	public function reviewflagAction() {
		$this->view->setTemplateAfter('admin-panel');
	}

    public function classifiedflagAction() {
        $this->view->setTemplateAfter('admin-panel');
    }
	
    public function jobflagAction() {
        $this->view->setTemplateAfter('admin-panel');
    }

    public function thingflagAction() {
        $this->view->setTemplateAfter('admin-panel');
    }

    public function approvethingflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Things::findFirst($reviewId);
            $review->status = 1;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '1';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

    public function approvejobflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Jobs::findFirst($reviewId);
            $review->status = 1;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '1';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

    public function approveautoflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Automotives::findFirst($reviewId);
            $review->status = 1;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '1';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

	public function approveflagAction($reviewId = null) {
		if ($this->request->isAjax()) {
			$arrResult = array();
				$reviewId = $this->request->getQuery('review_id');
				$flagId = $this->request->getQuery('flag_id');
			$review = Reviews::findFirst($reviewId);
			$review->visibility = 1;
			$actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
			if ($review->update()) {
				$result = $review->visibility;
			} else {
				$result = "failed";
			}
			$flag = Flags::findFirst($flagId);
			$flag->status = '1';
			if ($flag->update()) {
				$result2 = $flag->status;
			} else {
				$result2 = "failed";
			}
			$newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
			$arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
			
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
		}
	}
	
    public function rejectthingflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Things::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

    public function rejectjobflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Jobs::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

    public function rejectautoflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Automotives::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

	public function rejectflagAction($reviewId = null) {
		if ($this->request->isAjax()) {
			$arrResult = array();
				$reviewId = $this->request->getQuery('review_id');
				$flagId = $this->request->getQuery('flag_id');
			$review = Reviews::findFirst($reviewId);
			$review->visibility = 0;
			$actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
			if ($review->update()) {
				$result = $review->visibility;
			} else {
				$result = "failed";
			}
			$flag = Flags::findFirst($flagId);
			$flag->status = '2';
			if ($flag->update()) {
				$result2 = $flag->status;
			} else {
				$result2 = "failed";
			}
			$newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
			$arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
			
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
		}
	}
	
    public function cancelthingflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Things::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="approve('.$reviewId.','.$flagId.')"><i class="icon-checkmark-2"></i></span> | <a href="#" onclick="reject('.$reviewId.','.$flagId.')" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i></a>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }   
    }

    public function canceljobflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Jobs::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="approve('.$reviewId.','.$flagId.')"><i class="icon-checkmark-2"></i></span> | <a href="#" onclick="reject('.$reviewId.','.$flagId.')" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i></a>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }   
    }

    public function cancelautoflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Automotives::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="approve('.$reviewId.','.$flagId.')"><i class="icon-checkmark-2"></i></span> | <a href="#" onclick="reject('.$reviewId.','.$flagId.')" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i></a>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }   
    }

	public function cancelflagAction($reviewId = null) {
		if ($this->request->isAjax()) {
			$arrResult = array();
				$reviewId = $this->request->getQuery('review_id');
				$flagId = $this->request->getQuery('flag_id');
			$review = Reviews::findFirst($reviewId);
			$review->visibility = 0;
			$actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="approve('.$reviewId.','.$flagId.')"><i class="icon-checkmark-2"></i></span> | <a href="#" onclick="reject('.$reviewId.','.$flagId.')" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i></a>';
			if ($review->update()) {
				$result = $review->visibility;
			} else {
				$result = "failed";
			}
			$flag = Flags::findFirst($flagId);
			$flag->status = '2';
			if ($flag->update()) {
				$result2 = $flag->status;
			} else {
				$result2 = "failed";
			}
			$newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
			$arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
			
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
		}	
	}
}