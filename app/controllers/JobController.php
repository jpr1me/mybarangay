<?php
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;


class JobController extends ControllerBase{



    public function initialize()
    {
        parent::initialize();

    }

  public function getCitiesAction() {
        if ($this->request->isAjax()) {
            $arrCities = array();
             $stateCode = $this->request->getQuery('param1');
            $cities = Cities::find("state_code_id='" . $stateCode . "'");

            foreach ($cities as $city) {
                $arrCities[] = ['id' => $city->id, 'name' => $city->city_name];
            }
            $payload     = $arrCities; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }





    public function getStatesAction() {

        if ($this->request->isAjax()) {
            $arrStates = array();
            $countryCode = $this->request->getQuery('param');


            $states = States::find("country_code_id='" . $countryCode . "'");

            foreach ($states as $state) {
                $arrStates[] = ['id' => $state->id, 'name' => $state->state_name];
            }

            $payload     = $arrStates; //array(1, 2, 3);
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

          

            return $response;
        }
    }

    /**
     * [flagAction description]
     * @return [type] [description]
     */
    public function flagAction(){

        $this->view->disable();



        if($this->request->isAjax()) 
        {
      
            $review = $this->request->getQuery('review');
            $reviewer = $this->request->getQuery('reviewer');
            $flagger = $this->request->getQuery('flagger');
            $flag_type = $this->request->getQuery('flag_type');
            $referer = $this->request->getHTTPReferer (); 
            $controller = 'jobs';

            $newFlag = new Flags();
            $newFlag->created = date('Y-m-d H:i:s');
            $newFlag->modified = date('Y-m-d H:i:s');
            $newFlag->review_id = $review;
            $newFlag->member_id = $flagger;
            $newFlag->target_id = $reviewer;
            $newFlag->flag_type_id = $flag_type;
            $newFlag->location = $controller; 
            $newFlag->status = '0';
            $newFlag->page = $referer;

            if ($newFlag->create()) {
                $resultInfo = 'success';
            } else {
                $resultInfo = 'failed';
            }

            $arrayName = array('review' => $review, 'reviewer' => $reviewer, 'flagger' => $flagger, 'result' => $resultInfo);
            echo json_encode(array('result' => 'OK', 'items' => $arrayName));
        }



    }






    public function formValidate() {
        $validation = new Phalcon\Validation();
        $validation->add('position', new PresenceOf(array(
            'message' => 'position field is required'   
        )));
        $validation->add('job_category_id', new PresenceOf(array(
            'message' => 'job category field is required'   
        )));
        $validation->add('job_description', new PresenceOf(array(
            'message' => 'job description field is required'    
        )));
        $validation->add('requirements', new PresenceOf(array(
            'message' => 'requirements field is required'   
        )));
        $validation->add('benefits', new PresenceOf(array(
            'message' => 'benefits field is required'   
        )));
        $validation->add('how_to_apply', new PresenceOf(array(
            'message' => 'how to apply field is required'   
        )));
        $validation->add('salary_from', new PresenceOf(array(
            'message' => 'salary from field is required'    
        )));
        $validation->add('salary_to', new PresenceOf(array(
            'message' => 'salary to field is required'  
        )));
        $validation->add('company', new PresenceOf(array(
            'message' => 'company field is required'    
        )));
        $validation->add('website', new PresenceOf(array(
            'message' => 'website field is required'    
        )));
        $validation->add('telephone', new PresenceOf(array(
            'message' => 'telephone field is required'  
        )));
        $validation->add('email', new PresenceOf(array(
            'message' => 'email field is required'  
        )));
        $validation->add('street', new PresenceOf(array(
            'message' => 'street field is required' 
        )));
        $validation->add('city', new PresenceOf(array(
            'message' => 'position field is required'   
        )));
        $validation->add('country_id', new PresenceOf(array(
            'message' => 'position field is required'   
        )));

        return $validation->validate($_POST);
    
    }
    public function indexOldAction()
    {
        $searchWords = '';
        $jobs = array();
        if(isset($_GET["page"])){
            $currentPage = (int) $_GET["page"];
        } else {
            $currentPage = 1;
        }
        if ($this->request->isPost()) {
            $position = $this->request->getPost('position');
            $company = $this->request->getPost('company');
            $address = $this->request->getPost('address');
            $jobCategoryId = $this->request->getPost('job_category_id');
            $salaryFrom = $this->request->getPost('salary_from');
            $salaryTo = $this->request->getPost('salary_to');
            $this->view->position = $position;
            $jobCategory = JobCategories::findFirst($jobCategoryId);
            $country = Countries::findFirst(array('columns'    => '*', 
                                             'conditions' => 'country LIKE :country:', 
                                             'bind' => array('country' => $address)));
          
            $countryId = '';
            if($country) {
                $countryId = $country->id;
            }
            $conditions = ''; 
            $bind = array();
            if(!empty($position)) {
                $conditions .= ' OR position LIKE :position:';
                $bind['position'] = '%'.$position.'%';
                $searchWords .= ', '.$position;
            }
            if(!empty($company)) {
                $conditions .= ' OR company LIKE :company:';
                $bind['company'] = '%'.$company.'%';
                $searchWords .= ', '.$company;
            }
            if(!empty($address)) {
                $conditions .= ' OR street LIKE :street:';
                $bind['street'] = '%'.$address.'%';
                $conditions .= ' OR city LIKE :city:';
                $bind['city'] = '%'.$address.'%';
                $searchWords .= ', '.$address;
            }
            if(!empty($countryId)) {
                $conditions .= ' OR country_id LIKE :country_id:';
                $bind['country_id'] = $countryId;
                $searchWords .= ', '.$country->country;
            }
            if(!empty($jobCategoryId)) {
                $conditions .= ' OR job_category_id LIKE :job_category_id:';
                $bind['job_category_id'] = $jobCategoryId;
                $searchWords .= ', '.$jobCategory->name;
            }
            if(!empty($salaryFrom)) {
                $conditions .= ' OR salary_from LIKE :salary_from:';
                $bind['salary_from'] = $salaryFrom;
                $searchWords .= ', '.$salaryFrom;
            }
            if(!empty($salaryTo)) {
                $conditions .= ' OR salary_to LIKE :salary_to:';
                $bind['salary_to'] = $salaryTo;
                $searchWords .= ', '.$salaryTo;
            }
            $searchWords = substr($searchWords, 2); 
            $jobs = Jobs::find(array('columns'    => '*', 
                                     'conditions' => substr($conditions, 3), 
                                     'bind' => $bind
                                        ));
            
        } else {
            $jobs = Jobs::find();
        }

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $jobs,
                "limit"=> 10,
                "page" => $currentPage
            )
        );
        $page = $paginator->getPaginate();

        $this->view->setVar('searchWords', $searchWords);
        $this->view->setVar('jobs', $page);
        $jobCategories = JobCategories::find();
        $this->view->setVar('jobCategories', $jobCategories);
    }




   /**
    * [indexAction description]
    * @return [type] [description]
    */
   public function indexAction(){
        // set default1 template for new design template
        $this->view->setTemplateAfter('default1');



        $sql = '';
        $jobs = '';
        $name = '';
        $job_category_id = '';
        $btn_search = '';
        $sal_from = '';
        $sal_to = '';
        $city = '';
        $city_code = '';
      $sql = "SELECT * FROM Jobs ";
        if (isset($_GET['btn_search'])) {
            $btn_search = $this->request->getQuery('btn_search');
            $locations = [];
            $getters = [];
            $queries = [];
            $error = [];
            foreach ($_GET as $key => $value) {
               $temp = is_array($value) ? $value : trim($value) ;
               if(!empty($temp)){
                    list($key) = explode("-", $key);
                    if($key == 'name'){
                        array_push($locations, $value);
                    }
                    if(!in_array($key, $getters)){
                        $getters[$key] = $value;
                    }
               }
            }
            if(!empty($locations)){
                $loc_qry = implode(",", $locations);
            }
            if(!empty($getters)){
                foreach ($getters as $key => $value) {
                   ${$key} = $value;
                    switch ($key) {
                        case 'name':
                               array_push($queries, "(position LIKE '%$name%' OR company LIKE '%$name%'  )");
                            break;
                            case 'sal_from':
                            break;
                            case 'sal_to':
                            break;
                            case 'btn_search':
                                if(!empty($sal_from) AND !empty($sal_to)){
                                    array_push($queries, "(salary_from BETWEEN $sal_from AND $sal_to )");
                                }else{
                                    if(!empty($sal_from)){
                                        array_push($queries, "(salary_from > $sal_from )");
                                    }
                                    if(!empty($sal_to)){
                                        array_push($queries, "(salary_from < $sal_to )");
                                    }
                                }
                            break;
                            case 'job_category_id':
                                array_push($queries, "(job_category_id = $job_category_id)");
                            break;
                            case 'city_code':
                                array_push($queries, "(city = $city_code)");
                            break;
                            case 'city':
                            break;

                        default:
                            # code...
                            break;
                    }
                }
            }

            if(!empty($queries)){
                $sql .= " WHERE ";
                $i = 1;
                foreach ($queries as $query) {
                    if($i < count($queries)){
                        $sql .= $query." AND ";
                    }else{
                        $sql .= $query;
                    }
                    $i++;
                }
            }

        }

    $jobs = $this->modelsManager->executeQuery($sql);
    $jobCategories = JobCategories::find();
    $this->view->setVars([
    'jobs' => $jobs,
    'name' => $name,
    'job_category_id' => $job_category_id,
    'btn_search' => $btn_search,
    'sal_from' => $sal_from,
    'sal_to' => $sal_to,
    'city' => $city, 
    'city_code' => $city_code,
    'jobCategories' => $jobCategories
    ]);





    }


    public function viewAction($id = null)
    {
        $jobs = Jobs::findFirst($id);
        if(!$jobs) {
            return $this->response->redirect('jobs/search_jobs');
        }
        $this->view->setVar('jobs', $jobs);

        $member = Members::findFirst($jobs->member_id);
        $this->view->setVar('member', $member);

        $jobLogo = JobLogos::findFirst('job_id="'.$id.'"');
        if($jobLogo){
            $jobLogo = $jobLogo->file_path.$jobLogo->filename;
        } else {
            $jobLogo = 'http://placehold.it/200x200';
        }
        $this->view->setVar('jobLogo', $jobLogo);
    }
    
    public function view2Action($id = null)
    {
    $this->view->setTemplateAfter('default1');
        $jobs = Jobs::findFirst($id);
        if(!$jobs) {
            return $this->response->redirect('jobs/search_jobs');
        }
        $this->view->setVar('jobs', $jobs);

        $member = Members::findFirst($jobs->member_id);
        $this->view->setVar('member', $member);

        $jobLogo = JobLogos::findFirst('job_id="'.$id.'"');
        if($jobLogo){
            $jobLogo = $jobLogo->file_path.$jobLogo->filename;
        } else {
            $jobLogo = 'http://placehold.it/200x200';
        }
        $this->view->setVar('jobLogo', $jobLogo);
    }

    public function newAction()
    {
        $this->view->setTemplateAfter('default1');
        if ($this->request->isPost()) {
    
        $messages = $this->formValidate();
        if (count($messages)) {
            $this->view->disable();
            $errorMsg = '';
            foreach ($messages as $msg) {
              $errorMsg .= $msg . "<br>";       
            }
            // $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>'.$errorMsg);
        } 

                $userSession = $this->session->get("userSession");
                $jobs = new Jobs();
                $jobs->created = date('Y-m-d H:i:s'); 
                $jobs->modified = date('Y-m-d H:i:s'); 
                $jobs->member_id =  $userSession['id'];
                $jobs->position = $this->request->getPost('position');
                $jobs->job_category_id = $this->request->getPost('job_category_id');
                $jobs->job_description = $this->request->getPost('job_description');
                $jobs->requirements = $this->request->getPost('requirements');
                $jobs->benefits = $this->request->getPost('benefits');
                $jobs->salary_from = $this->request->getPost('salary_from');
                $jobs->salary_to = $this->request->getPost('salary_to');
                $jobs->company = $this->request->getPost('company');
                $jobs->website = $this->request->getPost('website');
                $jobs->telephone = $this->request->getPost('telephone');
                $jobs->email = $this->request->getPost('email');
                $jobs->how_to_apply = $this->request->getPost('how_to_apply');



                $jobs->street = $this->request->getPost('street');
                $jobs->state = $this->request->getPost('state');
                $jobs->city = $this->request->getPost('city');
                $jobs->country_code_id = $this->request->getPost('country_code_id');


                if($jobs->create()){

                $id = $jobs->id;
                $encoded = $this->request->getPost('image-data');
                $decoded = urldecode($encoded);
                $exp = explode(',', $decoded);
                $base64 = array_pop($exp);
                $data = base64_decode($base64);
                $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'job-'.$userSession['id'].'.png';
                $filePath = 'img/jobs/';

                $file = $filePath . $newFileName;
        
                        $jobLogos = new JobLogos();
                        $jobLogos->created = date('Y-m-d H:i:s'); 
                        $jobLogos->modified = date('Y-m-d H:i:s'); 
                        $jobLogos->member_id =  $userSession['id'];
                        $jobLogos->job_id =  $id;
                        
                    if (file_put_contents($file, file_get_contents($encoded))) {
                        
                        $jobLogos->file_path =  $filePath;
                        $jobLogos->filename =  $newFileName;

                        if($jobLogos->create()){
                        }
                    }
                return $this->response->redirect('job/view2/'.$jobs->id);
            }


        }
        $member = Members::findFirst($userSession['id']);
        $this->view->setVar('member', $member);
        $countries = Countries::find();
        $this->view->setVar('countries', $countries);
        $jobCategories = JobCategories::find();
        $this->view->setVar('jobCategories', $jobCategories);
    }

    public function updateAction($id = null)
    { 

           $this->view->setTemplateAfter('default1');     
        if ($this->request->isPost()) {

            $id = $this->request->getPost('id');
            $userSession = $this->session->get("userSession");
            $jobs = Jobs::findFirst($id); 
            $jobs->modified = date('Y-m-d H:i:s'); 
            $jobs->member_id =  $userSession['id'];
            $jobs->position = $this->request->getPost('position');
            $jobs->job_category_id = $this->request->getPost('job_category_id');
            $jobs->job_description = $this->request->getPost('job_description');
            $jobs->requirements = $this->request->getPost('requirements');
            $jobs->currency_type = $this->request->getPost('currency_type');
            $jobs->benefits = $this->request->getPost('benefits');
            $jobs->salary_from = $this->request->getPost('salary_from');
            $jobs->salary_to = $this->request->getPost('salary_to');
            $jobs->company = $this->request->getPost('company');
            $jobs->website = $this->request->getPost('website');
            $jobs->telephone = $this->request->getPost('telephone');
            $jobs->email = $this->request->getPost('email');
            $jobs->street = $this->request->getPost('street');
            $jobs->city = $this->request->getPost('city');
            $jobs->country_id = $this->request->getPost('country_id');
            $jobs->how_to_apply = $this->request->getPost('how_to_apply');
            if($jobs->update()){
                // $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>'.$jobs->position.' has been updated');
                // return $this->response->redirect('job/view2/'.$id);

                      $encoded = $this->request->getPost('image-data');
                       $filePath = 'img/jobs/';
                       $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'jb.png';
                       $file = $filePath . $newFileName;

                        $jobLogos = JobLogos::findFirst("job_id = '$id' ");
                        if($jobLogos){
                            $jobLogos->modified = date('Y-m-d H:i:s'); 
                            $jobLogos->file_path = $filePath;              
                            $jobLogos->member_id =  $userSession['id'];

                            $link = $filePath . $jobLogos->filename;
                            unlink($link);
                            if (file_put_contents($file, file_get_contents($encoded))) {
                               $jobLogos->file_path = $filePath;
                               $jobLogos->filename = $newFileName;
                            }

                            $jobLogos->update();   
                        }else{
                                $jobLogosP = new JobLogos();
                                $jobLogosP->created = date('Y-m-d H:i:s');
                                $jobLogosP->modified = date('Y-m-d H:i:s'); 
                                $jobLogosP->member_id = $userSession['id'];
                                $jobLogosP->job_id = $id;

                            if (file_put_contents($file, file_get_contents($encoded))) {
                                $jobLogosP->file_path = $filePath;
                                $jobLogosP->filename = $newFileName;
                            }

                            if($jobLogosP->create()){
                                echo 'ok';
                            }else{
                                echo 'error';
                            }
                        }


            }    






        }








        $this->view->setVar('job', Jobs::findFirst($id));
        $countries = Countries::find();
        $this->view->setVar('countries', $countries);
        $jobCategories = JobCategories::find();
        $this->view->setVar('jobCategories', $jobCategories);
        $jobLogo = JobLogos::findFirst('job_id="'.$id.'"');
        if($jobLogo){
            $jobLogo = $jobLogo->file_path.$jobLogo->filename;
        } else {
            $jobLogo = 'http://placehold.it/200x200';
        }
        $this->view->setVar('jobLogo', $jobLogo);
    }





    /**
     * [loadMoreEvent description]
     * @return [type] [description]
     */
    public function load_ajaxAction(){
        // This is an Ajax response so it doesn't generate any kind of view
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

        //if ajax request run
        if ($this->request->isAjax()) {



        }
    }






}

