<?php
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;

// use Helpers\OAuthException;
// use Helpers\OAuthConsumer;
// use Helpers\OAuthToken;
// use Helpers\OAuthSignatureMethod;
// use Helpers\OAuthSignatureMethod_HMAC_SHA1;
// use Helpers\OAuthSignatureMethod_PLAINTEXT;
// use Helpers\OAuthSignatureMethod_RSA_SHA1;
// use Helpers\OAuthRequest;
// use Helpers\OAuthServer;
// use Helpers\OAuthDataStore;
// use Helpers\OAuthUtil;


//use Helpers\Sample;
//use Helpers\YelpApi as Yeppie;

class PaymentController extends ControllerBase {
    


    public function initialize()
    {
        parent::initialize();

    }

 
    public function templateAction() {
	   // do nothing...,
    }

    public function pricetableAction($member_id = null) {
      $this->view->setVar('memberId' , $member_id);
    }

    public function payAction($plan = null) {
      $this->view->disable();
      if (!$plan) {
        echo 'choose at least one plan';
      } else {

        $decryptedPlan = $this->crypt->decryptBase64($plan);

        echo $decryptedPlan;
      }
    }

    public function processAction($paymentType = null) {

      $userSession = $this->session->get('userSession');
      
      if ($paymentType == null) {
        $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Choose one plan');
        return $this->response->redirect('payment/pricetable');
      }

      if ($this->request->isSecureRequest() == false) {
        return $this->response->redirect('https://' . $this->url->getBaseUri() . 'payment/process/' . $paymentType);
      } else {
        return $this->response->redirect('payment/proceed/'.$paymentType);
      }
    }

    public function proceedAction($paymentType = null) {
      
      $userSession = $this->session->get('userSession');

      $userId = $userSession['id'];
      
      $member = Members::findFirst("id='".$userId."'");

      if($paymentType == null) {
        $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Choose one plan');
        return $this->response->redirect('payment/pricetable');
      }

      switch ($paymentType) {
        case '1':
          $value = '29.99';
          break;
        case '2':
          $value = '39.99';
          break;
        case '3':
          $value = '99.99';
          break;
        default:
          $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Choose one plan');  
          return $this->response->redirect('payment/pricetable');
          break;
      }

      $this->view->disable();
      echo 'amount: ' . $value . '<br>';
      echo 'payment type: ' . $paymentType . '<br>';
      echo 'user_id: ' . $userId . '<br>';

      echo 'firstname: ' . $member->first_name. '<br>';
      echo 'lastname: ' . $member->last_name;
      // $this->view->setVars([
      //     'value' => $value,
      //     'paymentType' => $paymentType,
      //     'userId' => $userId
      // ]);      

    }
}