<?php


class Cities extends \Phalcon\Mvc\Model
{

    //PIVOT TABLE 
    public $id;
    public $country_code_id;
    public $state_code_id;
    public $city_name;

    public function initialize()
    {
        $this->belongsTo("country_code_id", "CountryCodes", "id");
        $this->belongsTo("state_code_id", "States", "id");
    }



    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'state_code_id' => 'state_code_id', 
            'country_code_id' => 'country_code_id',
            'city_name' => 'city_name'
        );
    }

}