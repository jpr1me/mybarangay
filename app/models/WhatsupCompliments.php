<?php

class WhatsupCompliments extends \Phalcon\Mvc\Model
{

    public $id;

    public $created;

    public $modified;

    public $member_id;

    public $rate;

    public $whatsup_topic_id;

    public function initialize()
    {
         $this->belongsTo("whatsup_topic_id", "WhatsupTopics", "id");
         $this->belongsTo("member_id", "Members", "id");
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'created' => 'created',
            'modified' => 'modified',
            'member_id' => 'member_id',
            'rate' => 'rate',
            'whatsup_topic_id' => 'whatsup_topic_id'
        );
    }

}
