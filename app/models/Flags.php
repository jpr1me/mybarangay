<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class Flags extends \Phalcon\Mvc\Model
{

    public $id;
    public $created;
    public $modified;
    public $review_id;
    public $member_id;
    public $target_id;
    public $flag_type_id;
    public $location;
    public $page;
    public $status;

    public function initialize()
    {
        $this->belongsTo("flag_type_id", "FlagTypes", "id");
        $this->belongsTo("member_id", "Members", "id");
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'created' => 'created',
            'modified' => 'modified',
            'review_id' => 'review_id',
            'member_id' => 'member_id',
            'target_id' => 'target_id',
            'flag_type_id' => 'flag_type_id',
            'location' => 'location',
            'page' => 'page',
            'status' => 'status'
        );
    }

}
