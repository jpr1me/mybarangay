<?php

class WhatsupReplies extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var text
     */
    public $content;

    /**
     *
     * @var date
     */
    public $created_at;


    /**
     *
     * @var integer
     */
    public $whatsup_topic_id;


    /**
     *
     * @var integer
     */
    public $member_id;



    /**
     * [initialize description]
     * @return [type] [description]
     */
    public function initialize(){

        $this->belongsTo("whatsup_topic_id", "WhatsupTopics", "id");


    }






}