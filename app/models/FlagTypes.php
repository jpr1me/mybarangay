<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class FlagTypes extends \Phalcon\Mvc\Model
{

    public $id;
    public $created;
    public $modified;
    public $detail;


    public function initialize()
    {
        $this->hasMany("id", "Flags", "flag_type_id");
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'created' => 'created',
            'modified' => 'modified',
            'detail' => 'detail'
        );
    }

}
