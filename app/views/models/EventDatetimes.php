<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class EventDatetimes extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

      /**
     *
     * @var integer
     */
    public $event_id;



      /**
     *
     * @var integer
     */
    public $from_time;

     /**
     *
     * @var integer
     */
    public $to_time;


    public function initialize()
    {
        $this->belongsTo("event_category_id", "EventsCategories", "id");
        $this->belongsTo("country_id", "Countries", "id");
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'event_id' => 'event_id',
            'from_time' => 'from_time',
            'to_time' => 'to_time'
        );
    }

}
