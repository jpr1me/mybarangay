<?php 


namespace Helpers;


class ZoidHelpers {

     /**
      * Pending for : FORM BUILDER
      *
    * $category = array('0' => 'Monthly', '1' => 'Daily','2' => 'Piece-rate');
    * ZoidHelpers::StaticDropDown('add_category', $category, $x['Category']);
      * 
      * [StaticDropDown description]
      * @param [type] $name  [description]
      * @param [type] $array [description]
      * @param [type] $v     [description]
      */
     
    public static function staticDropDown($name, $array , $v = null, $default = ' -- select -- '){
        echo "<select id=\"$name\" name=\"$name\" class=\" form-control\"   >";
        echo  "<option value=\"\">$default</option>";
        foreach ($array as $key => $value) {
                echo '<option value="'.$key.'"';
                if(trim($key) == $v){echo ' selected';}
                echo '>'. $value . '</option>';
        }
        echo "</select>";

     }

   /**
     * [DynamicDropDown description]
     * @param [type] $zoid [description]
     * @param [type] $name [description]
     * @param string $opt  [description]
     * @param [type] $code [description]
     * @param [type] $desc [description]
     */
    public static function dynamicDropDown($zoid, $name, $opt = ''){
        echo "<select id=\"$name\" name=\"$name\" class=\"combobox form-control\" >";
        echo  "<option value=\"\">- select -</option>";
    foreach ($zoid as $condition) {
      if ($condition->id == $opt) {
        echo '<option value="'.$condition->id.'" selected>'.$condition->name.'</option>';
      } else {
        echo '<option value="'.$condition->id.'">'.$condition->name.'</option>';
      }
    }
    echo '</select>';
  }





  public static function paginate($pageNum,$numOfPages,$rowsReturned,$links){

    if ($pageNum == 0){$pageNum = 1;}
    $prev = $pageNum - 1;  
    $next = $pageNum + 1;       
    $stages = 3;

    $LastPagem1 = $numOfPages - 1;
    $paginate = '';



    if($numOfPages > 1)
      { //start of pagination

    
          $paginate .= "<ul class=\"pagination pull-right\">";

          // PREVIOUS BUTTON
          if ($pageNum > 1){
              $paginate.= "<li><a   href=\"$links?page=$prev\"   aria-label=\"Previous\">Previous</a></li>";
          }else{
              $paginate.= "<li class=\"disabled\"><a href=\"#\" aria-label=\Previous\"><span aria-hidden=\"true\">&laquo;</span></a></li>";   }
          // START PAGES
          if ($numOfPages < 7 + ($stages * 2))  // Not enough pages to breaking it up
          {   
              for ($counter = 1; $counter <= $numOfPages; $counter++)
              {
                  if ($counter == $pageNum){
                      $paginate.= "<li class=\"active\"><span class=\"current\">$counter</span></li>";
                  }else{
                      $paginate.= "<li><a  href=\"$links?page=$counter\"  >$counter</a></li>";
                  }                    
              }
          }
      elseif($numOfPages > 5 + ($stages * 2))   // pag few page na HIDE
          {
              // START only hide later pages
              if($pageNum < 1 + ($stages * 2))       
              {
                  for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
                  {
                      if ($counter == $pageNum){
                          $paginate.= "<li class=\"active\"><span class=\"current\">$counter</span></li>";
                      }else{
                          $paginate.= "<li><a href=\"$links?page=$counter\"  >$counter</a></li>";}                    
                  }
                  $paginate.= "<li><a href=\"\">...</a></li>";
                  $paginate.= "<li><a href=\"$links?page=$LastPagem1\"  >$LastPagem1</a></li>";
                  $paginate.= "<li><a href=\"$links?page=$numOfPages\"  >$numOfPages</a></li>";       
              }
              //MIDDLE HIDE  hide ung front and back
              elseif($numOfPages - ($stages * 2) > $pageNum && $pageNum > ($stages * 2))
              {
                  $paginate.= "<li><a href='javascript:void(0);' OnClick='$x(1)' >1</a></li>";
                  $paginate.= "<li><a href='javascript:void(0);' OnClick='$x(2)' >2</a></li>";
                  $paginate.= "<li><a href=\"#\">...</a></li>";
                  for ($counter = $pageNum - $stages; $counter <= $pageNum + $stages; $counter++)
                  {
                      if ($counter == $pageNum){
                          $paginate.= "<li class=\"active\"><span class=\"current\">$counter</span></li>";
                      }else{
                          $paginate.= "<li><a href=\"$links?page=$counter\"  >$counter</a></li>";}                    
                  }
                  $paginate.= "<li><a href=\"#\">...</a></li>";
                  $paginate.= "<li><a href=\"$links?page=$LastPagem1\"  >$LastPagem1</a></li>";
                  $paginate.= "<li><a href=\"$links?page=$numOfPages\"  >$numOfPages</a></li>";       
              }
              // Hide ung mga early pages
              else
              {
                  $paginate.= "<li><a href=\"$links?page=1\" >1</a></li>";
                  $paginate.= "<li><a href=\"$links?page=2\" >2</a></li>";
                  $paginate.= "<li><a href=\"#\">...</a></li>";
                  for ($counter = $numOfPages - (2 + ($stages * 2)); $counter <= $numOfPages; $counter++)
                  {
                      if ($counter == $pageNum){
                          $paginate.= "<li class=\"active\"><span class=\"current\">$counter</span></li>";
                      }else{
                          $paginate.= "<li><a href=\"$links?page=$counter\"  >$counter</a></li>";}                    
                  }
              }
          }
          // NEXT BUTTON
          if ($pageNum < $counter - 1){
              $paginate.= "<li><a  href=\"$links?page=$next\" >next</a></li>";
          }else{
              $paginate.= "<li><span class=\"disabled\">next</span></li>";
              }
          $paginate.= "</ul>";       
      
  

      }//end of pagination

 return $paginate;





  }




  
}