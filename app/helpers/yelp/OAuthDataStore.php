<?php
namespace Helpers\YelpApi;
use Helpers\YelpApi\OAuthException;
use Helpers\YelpApi\OAuthConsumer;
use Helpers\YelpApi\OAuthToken;
use Helpers\YelpApi\OAuthSignatureMethod;
use Helpers\YelpApi\OAuthSignatureMethod_HMAC_SHA1;
use Helpers\YelpApi\OAuthSignatureMethod_PLAINTEXT;
use Helpers\YelpApi\OAuthSignatureMethod_RSA_SHA1;
use Helpers\YelpApi\OAuthRequest;
use Helpers\YelpApi\OAuthServer;
use Helpers\YelpApi\OAuthUtil;

class OAuthDataStore {
  function lookup_consumer($consumer_key) {
    // implement me
  }
  function lookup_token($consumer, $token_type, $token) {
    // implement me
  }
  function lookup_nonce($consumer, $token, $nonce, $timestamp) {
    // implement me
  }
  function new_request_token($consumer, $callback = null) {
    // return a new token attached to this consumer
  }
  function new_access_token($token, $consumer, $verifier = null) {
    // return a new access token attached to this consumer
    // for the user associated with this token if the request token
    // is authorized
    // should also invalidate the request token
  }
}

?>